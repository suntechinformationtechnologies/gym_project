-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 30, 2022 at 06:07 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gym_v2`
--

-- --------------------------------------------------------

--
-- Table structure for table `main_cat`
--

CREATE TABLE `main_cat` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `main_cat`
--

INSERT INTO `main_cat` (`id`, `name`) VALUES
(1, 'Arms'),
(2, 'Back'),
(4, 'Legs'),
(5, 'Shoulders');

-- --------------------------------------------------------

--
-- Table structure for table `playlist`
--

CREATE TABLE `playlist` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `w_h` int(11) NOT NULL DEFAULT 0,
  `w_m` int(11) NOT NULL DEFAULT 0,
  `w_s` int(11) NOT NULL DEFAULT 0,
  `r_h` int(11) NOT NULL DEFAULT 0,
  `r_m` int(11) NOT NULL DEFAULT 0,
  `r_s` int(11) NOT NULL DEFAULT 0,
  `nor` int(11) NOT NULL DEFAULT 1,
  `eor_h` int(11) NOT NULL DEFAULT 0,
  `eor_m` int(11) NOT NULL DEFAULT 0,
  `eor_s` int(11) NOT NULL DEFAULT 0,
  `hb_h` int(11) NOT NULL DEFAULT 0,
  `hb_m` int(11) NOT NULL DEFAULT 0,
  `hb_s` int(11) NOT NULL DEFAULT 0,
  `eof_slot` int(11) NOT NULL DEFAULT 0,
  `thumbnail` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `playlist`
--

INSERT INTO `playlist` (`id`, `date`, `name`, `w_h`, `w_m`, `w_s`, `r_h`, `r_m`, `r_s`, `nor`, `eor_h`, `eor_m`, `eor_s`, `hb_h`, `hb_m`, `hb_s`, `eof_slot`, `thumbnail`) VALUES
(33, '2022-01-30 10:36:51', 'test playlist 1', 0, 0, 60, 0, 0, 30, 2, 0, 0, 30, 0, 0, 15, 3, '36206538_30-01-2022_10-36-51pm.png');

-- --------------------------------------------------------

--
-- Table structure for table `playlist_has_video`
--

CREATE TABLE `playlist_has_video` (
  `id` int(11) NOT NULL,
  `playlist_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sub_cat`
--

CREATE TABLE `sub_cat` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `main_cat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sub_cat`
--

INSERT INTO `sub_cat` (`id`, `name`, `main_cat_id`) VALUES
(1, 'Squats a', 4),
(2, 'Lunges', 4),
(3, 'Lunges', 1),
(77, 'sub 1', 5);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `un` varchar(255) NOT NULL,
  `pw` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `link` text NOT NULL,
  `thumbnail` text DEFAULT NULL,
  `filesize` varchar(45) DEFAULT NULL,
  `sub_cat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `main_cat`
--
ALTER TABLE `main_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `playlist`
--
ALTER TABLE `playlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `playlist_has_video`
--
ALTER TABLE `playlist_has_video`
  ADD PRIMARY KEY (`playlist_id`,`video_id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_playlist_has_video_video1_idx` (`video_id`),
  ADD KEY `fk_playlist_has_video_playlist_idx` (`playlist_id`);

--
-- Indexes for table `sub_cat`
--
ALTER TABLE `sub_cat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sub_cat_main_cat1_idx` (`main_cat_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_video_sub_cat1_idx` (`sub_cat_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `main_cat`
--
ALTER TABLE `main_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `playlist`
--
ALTER TABLE `playlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `playlist_has_video`
--
ALTER TABLE `playlist_has_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `sub_cat`
--
ALTER TABLE `sub_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `playlist_has_video`
--
ALTER TABLE `playlist_has_video`
  ADD CONSTRAINT `fk_playlist_has_video_playlist` FOREIGN KEY (`playlist_id`) REFERENCES `playlist` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_playlist_has_video_video1` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sub_cat`
--
ALTER TABLE `sub_cat`
  ADD CONSTRAINT `fk_sub_cat_main_cat1` FOREIGN KEY (`main_cat_id`) REFERENCES `main_cat` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `video`
--
ALTER TABLE `video`
  ADD CONSTRAINT `fk_video_sub_cat1` FOREIGN KEY (`sub_cat_id`) REFERENCES `sub_cat` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
