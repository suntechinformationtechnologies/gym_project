-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 28, 2021 at 06:19 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gym`
--

-- --------------------------------------------------------

--
-- Table structure for table `main_cat`
--

CREATE TABLE `main_cat` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `main_cat`
--

INSERT INTO `main_cat` (`id`, `name`) VALUES
(1, 'Arms'),
(2, 'Back'),
(4, 'Legs'),
(5, 'Shoulders'),
(25, 'v');

-- --------------------------------------------------------

--
-- Table structure for table `playlist`
--

CREATE TABLE `playlist` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `w_h` int(11) NOT NULL DEFAULT 0,
  `w_m` int(11) NOT NULL DEFAULT 0,
  `w_s` int(11) NOT NULL DEFAULT 0,
  `r_h` int(11) NOT NULL DEFAULT 0,
  `r_m` int(11) NOT NULL DEFAULT 0,
  `r_s` int(11) NOT NULL DEFAULT 0,
  `thumbnail` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `playlist`
--

INSERT INTO `playlist` (`id`, `date`, `name`, `w_h`, `w_m`, `w_s`, `r_h`, `r_m`, `r_s`, `thumbnail`) VALUES
(7, '2021-12-22 12:41:47', 'my playlist second ', 0, 5, 0, 0, 1, 0, '30757350_22-12-2021_12-41-47pm.png'),
(14, '2021-12-23 09:31:35', 'test 2', 0, 2, 0, 0, 3, 0, '11847070_23-12-2021_09-31-35am.png');

-- --------------------------------------------------------

--
-- Table structure for table `playlist_has_video`
--

CREATE TABLE `playlist_has_video` (
  `playlist_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `playlist_has_video`
--

INSERT INTO `playlist_has_video` (`playlist_id`, `video_id`) VALUES
(7, 8),
(7, 9),
(7, 10),
(14, 1),
(14, 4),
(14, 8),
(14, 9);

-- --------------------------------------------------------

--
-- Table structure for table `sub_cat`
--

CREATE TABLE `sub_cat` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `main_cat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sub_cat`
--

INSERT INTO `sub_cat` (`id`, `name`, `main_cat_id`) VALUES
(1, 'Squats a', 4),
(2, 'Lunges', 4),
(3, 'Lunges', 1),
(27, 'sub 1', 1),
(70, 'a', 25),
(71, 'a', 5);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `un` varchar(255) NOT NULL,
  `pw` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `link` text NOT NULL,
  `thumbnail` text DEFAULT NULL,
  `filesize` varchar(45) DEFAULT NULL,
  `sub_cat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `name`, `date`, `link`, `thumbnail`, `filesize`, `sub_cat_id`) VALUES
(1, 'Video 202', '2021-12-15 07:45:32', 'video-1111.mp4', NULL, NULL, 1),
(2, 'Video 203', '2021-12-15 07:45:32', 'video-1111.mp4', NULL, NULL, 1),
(3, 'Video 204', '2021-12-15 07:45:32', 'video-1111.mp4', NULL, NULL, 1),
(4, 'Video 205', '2021-12-15 07:45:32', 'video-1111.mp4', NULL, NULL, 1),
(5, 'Video 206', '2021-12-15 07:45:32', 'video-1111.mp4', NULL, NULL, 1),
(6, 'Video 207', '2021-12-15 07:45:32', 'video-1111.mp4', NULL, NULL, 1),
(7, 'Video 208', '2021-12-15 07:45:32', 'video-1111.mp4', NULL, NULL, 1),
(8, 'Video 209', '2021-12-15 07:45:32', 'video-1111.mp4', NULL, NULL, 1),
(9, 'Video 302', '2021-12-15 07:48:41', 'video-2222.mp4', NULL, NULL, 2),
(10, 'Video 303', '2021-12-15 07:48:41', 'video-2222.mp4', NULL, NULL, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `main_cat`
--
ALTER TABLE `main_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `playlist`
--
ALTER TABLE `playlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `playlist_has_video`
--
ALTER TABLE `playlist_has_video`
  ADD PRIMARY KEY (`playlist_id`,`video_id`),
  ADD KEY `fk_playlist_has_video_video1_idx` (`video_id`),
  ADD KEY `fk_playlist_has_video_playlist_idx` (`playlist_id`);

--
-- Indexes for table `sub_cat`
--
ALTER TABLE `sub_cat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sub_cat_main_cat1_idx` (`main_cat_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_video_sub_cat1_idx` (`sub_cat_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `main_cat`
--
ALTER TABLE `main_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `playlist`
--
ALTER TABLE `playlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `sub_cat`
--
ALTER TABLE `sub_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `playlist_has_video`
--
ALTER TABLE `playlist_has_video`
  ADD CONSTRAINT `fk_playlist_has_video_playlist` FOREIGN KEY (`playlist_id`) REFERENCES `playlist` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_playlist_has_video_video1` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sub_cat`
--
ALTER TABLE `sub_cat`
  ADD CONSTRAINT `fk_sub_cat_main_cat1` FOREIGN KEY (`main_cat_id`) REFERENCES `main_cat` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `video`
--
ALTER TABLE `video`
  ADD CONSTRAINT `fk_video_sub_cat1` FOREIGN KEY (`sub_cat_id`) REFERENCES `sub_cat` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
