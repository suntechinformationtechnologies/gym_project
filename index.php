<?php
session_start();
if(isset($_SESSION['login'])){
    if($_SESSION['login'] != "skjhgfdsjgfs7wrbfhjkmzdbt7wrto93"){
        header("Location:login");
    }
}else{
    header("Location:login");
}
require_once "inc/config.php";
require_once "inc/Video.php";
require_once "inc/PlayList.php";
function rdr($loc){
    echo '<script>window.location.replace("'.$loc.'");</script>';
}
$eofsData = ['End of 1st slot','End of 2nd slot','End of 3rd slot','End of 4th slot','End of 5th slot','End of 6th slot','End of 7th slot','End of 8th slot'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Gym Demo version v1.1.1</title>   
    <link href="bootstrap-5.0.2-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/fonts.css" rel="stylesheet">
    <link href="css/layout.css" rel="stylesheet">
    <link href="css/icons.css" rel="stylesheet">
    <link href="css/colors.css" rel="stylesheet">
    <link href="css/buttons.css" rel="stylesheet">
    <!--link href="css/style.css" rel="stylesheet"-->
    <!--link rel="stylesheet" href="plugings/skin-video-player/css/ckin.css"-->
    <link
  href="https://unpkg.com/video.js@7/dist/video-js.min.css"
  rel="stylesheet"
/>

<!-- City -->
<link
  href="https://unpkg.com/@videojs/themes@1/dist/city/index.css"
  rel="stylesheet"
/>

    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.js" integrity="sha512-eSeh0V+8U3qoxFnK3KgBsM69hrMOGMBy3CNxq/T4BArsSQJfKVsKb5joMqIPrNMjRQSTl4xG8oJRpgU2o9I7HQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css" integrity="sha512-yVvxUQV0QESBt1SyZbNJMAwyKvFTLMyXSyBHDO4BG5t7k/Lw34tyqlSDlKIrIENIzCl+RVUNjmCPG+V/GMesRw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="plugings/chosen/chosen-dark.css">
<link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
</head>
<body>
    <nav class="navbar sticky-top navbar-expand-sm navbar-cutom-1">
        <div class="container-fluid">
          <a class="navbar-brand mt-1 ms-2" href="">
              <span class="i-line-1"></span>
              <span class="i-line-1"></span>
              <span class="i-line-1"></span>
          </a>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link ms-5" aria-current="page" href="">
                    <span class="band ms-2"></span>
                </a>
              </li>
            </ul>
            <div class="d-flex">
                <span class="user-icon"></span>
            </div>
          </div>
        </div>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <nav class="col-13">
                <div class="side-nav mt-4 position-fixed">
                    <a href="" class="home-ico-back ms-3 mb-1">
                        <img src="img/home-ico.png"/>
                    </a>
                    <a href="" class="side-ico ms-3 mt-4">
                        <img src="img/icon.png"/>
                    </a>
                    <a href="logout" class="home-ico-back ms-3 mt-4" style="text-decoration: none;">
                        <i class="fa fa-power-off bg-red-0-0" aria-hidden="true"></i>
                    </a>
                </div>
            </nav>
            <div class="col-14">
                <nav class="menu d-none" id="menu-1">
                    <ul>
                      <!--li>
                        <button data-vid="null" id="menu-add-play">Add to a playlist</button>
                      </li>
                      <li>
                        <button data-vid="null" id="menu-move-vid">Move video</button>
                      </li-->
                      <li>
                        <button data-vid="null" data-title="" id="menu-edit-tit" data-bs-toggle="modal" data-bs-target="#edit-video-title-model">Edit Video title</button>
                      </li>
                      <li>
                        <button data-vid="null" data-title="" data-src="" id="menu-dele-vid">Delete Video</button>
                      </li>
                    </ul>
                  </nav>
                <div class="row"> 
                    <div class="main-holder-1">                     
                        <div class="holder-2 mt-4 ms-5 bg-dark-0">
                            <div class="d-flex justify-content-between">
                                <h6 class="tt-1 pt-4 ms-5">Exercises</h6>
                                <div class="mt-4 me-2">
                                    <div class="my-input-group">
                                        <input id="serach-in-text" type="text" class="my-input-ser" placeholder="Search"/>
                                        <button id="search-btn-ele" class="my-input-ser-but"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        <button id="search-can-btn" class="my-input-ser-but d-none"><i class="fa fa-times-circle" aria-hidden="true"></i></button>
                                        <button class="ms-1 mybtn-2" data-bs-toggle="modal" data-bs-target="#exercise-add-new-model">Add Exercise</button>
                                    </div>
                                    
                                </div>
                            </div>
                                <div class="ms-5 mt-4" style="display: inline-block;">
                                    <!--button class="mybtn-1 me-1">Arms</button>
                                    <button class="mybtn-1 me-1">Back</button>
                                    <button class="mybtn-1 me-1">Chest</button>
                                    <button class="mybtn-1 me-1 active-1">Legs</button>
                                    <button class="mybtn-1 me-1">Sholders</button-->
                                    <?php
                                    try{
                                        $video = new Video($db);
                                        foreach($video->getCategories() as $mCatkey => $mcat){
                                            if(!isset($_GET['c'])){ 
                                                //header("location:test?c=".$mcat['id']);
                                                rdr("?c=".$mcat['id']);
                                            }else{
                                                if($_GET['c'] == $mcat['id']){
                                                    echo '<button data-cid="'.$mcat['id'].'" id="mcat-change-btn-'.$mcat['id'].'" class="mcat-change-btn mybtn-1 me-1 active-1">'.$mcat['name'].'</button>';
                                                }else{
                                                    echo '<button data-cid="'.$mcat['id'].'" id="mcat-change-btn-'.$mcat['id'].'" class="mcat-change-btn mybtn-1 me-1">'.$mcat['name'].'</button>';
                                                }
                                            }
                                            
                                        }
                                    }catch(Exception $ex){}
                                    ?>
                                    <span class="icon-2" data-bs-toggle="modal" data-bs-target="#exercise-areas-model">
                                        <img src="img/pencil.png" class="pencil-ico"/>
                                    </span> 
                                </div> 
                            <div class="holder-2-scroll do-nicescrol mt-2">
                                <div class="hol-23" id="video-content-top-1">  
                                    <?php                                        
                                        try{
                                            $video = new Video($db);
                                            foreach($video->getSubCategories($_GET['c']) as $sCatKey => $scat){
                                                echo '<div class="card-1">
                                                        <p class="tt-2">'.$scat['name'].'</p>
                                                        <span class="vid-ico-bg-1 open-new-excer-model-by-sub" data-sid="'.$scat['id'].'" data-bs-toggle="modal" data-bs-target="#exercise-add-new-model">
                                                            <i class="fa fa-plus vid-plus-1" aria-hidden="true"></i>
                                                        </span>
                                                        <div class="card-holder-2 do-nicescrol-2">';
                                                            try{
                                                                foreach($video->getVidos($scat['id']) as $vKey => $vid){
                                                                   echo '<div class="card-2 do-nicescrol-2 drag-test" data-vid="'.$vid['id'].'">
                                                                   <div class="p-1">
                                                                       <video 
                                                                            id="my-video"
                                                                            class="video-js vjs-theme-city"
                                                                            controls
                                                                            preload="auto"
                                                                            width="100%"
                                                                            height="128px"
                                                                            data-setup="{}" src="source/videos/'.$vid['link'].'" data-color="#ffffff"></video>
                                                                   </div>
                                                                   <span class="line-1"></span>
                                                                   <span class="tt-3">'.$vid['name'].'</span>
                                                                   <span class="vid-ico-bg-2 vid-right-click" data-src="source/videos/'.$vid['link'].'" data-title="'.$vid['name'].'" data-vid="'.$vid['id'].'">
                                                                       <!--i class="fa fa-ellipsis-h text-light" aria-hidden="true"></i-->
                                                                       <i class="fa fa-circle fa-sm text-light" style="font-size:5px;padding:0.66px;margin-top:2px" aria-hidden="true"></i>
                                                                       <i class="fa fa-circle fa-sm text-light" style="font-size:5px;padding:0.66px;margin-top:2px" aria-hidden="true"></i>
                                                                       <i class="fa fa-circle fa-sm text-light" style="font-size:5px;padding:0.66px;margin-top:2px" aria-hidden="true"></i>
                                                                   </span>                               
                                                               </div>';
                                                                }
                                                            }catch(Exception $ex){}
                                                echo        '</div>
                                                    </div>';
                                                
                                            }
                                        }catch(Exception $ex){}
                                    ?>
                                    <div class="card-1 pointer" data-bs-toggle="modal" data-bs-target="#exercise-type-model">
                                        <span class="text-cen-1">Add New & Edit</span>
                                    </div>                                     
                                </div>  
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-6">
                        <h2 class="tt-1 ms-5 ps-5 pt-3 mt-5">My Playlists</h2>
                    </div>
                    <div class="col-6 d-flex flex-row-reverse">
                        <button class="mybtn-2 mt-5 mb-3 me-4" data-bs-toggle="modal" data-bs-target="#new-playlist">Add Playlist</button>
                    </div>
                </div> 
                <div class="row mb-4">
                    <div class="playlist-holder do-nicescrol-3">
                        
                        <!--div class="col-12">
                            <div class="card bg-dark-0 mb-2 playlist-card-1 ms-5">
                                <div class="card-body bg-dark-0 playlist-card-1 p-0">
                                    <div class="bg-dark-0 p-3 playlist-card-1">                                    
                                        <div class="d-flex flex-row plalist-expand-dec" data-my="101" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            <div class="playlist-ico-1">
                                                <img src="img/a.png" class=""/>
                                            </div>
                                            <p class="playlist-name">Play List 101</p>
                                            <div class="d-flex invisible" id="target-ex-1-101">
                                                <p class="tt-4 ms-5 ps-4">Workout Time <span class="text-light">00 S</span></p>
                                                <p class="tt-4 ms-4">Rest Time <span class="text-light">00 S</span></p>                                                
                                            </div>
                                        </div>
                                        <span class="playlist-edit-ico invisible" id="target-ex-2-101">
                                            <img src="img/pencil.png" class="pencil-ico"/>
                                        </span>                                        
                                    </div>
                                    <div class="collapse" id="collapseExample">
                                        This is some text within a card body.<br/>
                                        This is some text within a card body.<br/>
                                        This is some text within a card body.<br/>
                                    </div>
                                </div>
                            </div>
                        </div-->
                        <?php 
                        try{
                            $playlist = new PlayList($db);
                            foreach($playlist->getAll() as $plaKey => $play) { 
                                $i = $play['id'];
                                
                                echo '
                                <div class="col-12">
                                    <form method="post" action="" enctype="multipart/form-data" id="plalist-updating-form-'.$i.'">
                                    <div class="card bg-dark-0 mb-2 playlist-card-1 ms-5">
                                        <div class="card-body bg-dark-0 playlist-card-1 p-0">
                                            <div class="bg-dark-0 p-3 playlist-card-1">                                    
                                                <div class="d-flex flex-row plalist-expand-dec" data-my="'.$i.'" id="playcollap-'.$i.'" data-toggle="collapse" href="#collapseExample-'.$i.'" role="button" aria-expanded="false" aria-controls="collapseExample">
                                                    <div class="d-flex playlist-ico-1 animate__animated animate__faster off-collapsable" id="target-img-1-'.$i.'">
                                                        <img src="'.$play['thumbnail'].'" class=""/>
                                                    </div>                                                    
                                                    <p class="playlist-name" id="playlist-name-'.$i.'">'.$play['name'].'</p>
                                                    <div class="d-none animate__animated animate__faster off-collapsable play-ico-holder-22" my-data="'.$i.'" id="playlist-ico-edit-'.$i.'">
                                                        <span class="playlist-ico-remover" data-in="'.$i.'" my-data="'.$i.'" id="vid-drop-rb-">
                                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                                        </span> 
                                                        <div class="playlist-ico-1 upd-playlist-ico-1 mt-1 me-1" my-data="'.$i.'" id="target-img-ico-2-'.$i.'">
                                                            <span class="d-none" id="iconset-update-dataset-'.$i.'" data-oldimg="'.$play['thumbnail'].'" data-rem="0" data-up="0"></span>
                                                            <img src="'.$play['thumbnail'].'" class="" id="target-img-ico-3-'.$i.'"/>
                                                            <input type="file" onchange="previewFileUpIco(this)" my-data="'.$i.'" accept=".jpg,.jpeg,.png,.JPG,.JPEG,.PNG" name="playlist-ico-update-'.$i.'" id="playlist-ico-update-'.$i.'" class="d-none">
                                                        </div>
                                                    </div>
                                                    <input type="text" style="height:40px" class="ms-5 mt-2 mb-2 my-input-1 ps-3 pe-3 d-none" value="'.$play['name'].'" id="playlist-name-edit-'.$i.'">
                                                    <div class="d-none animate__animated animate__faster" id="target-ex-1-'.$i.'">
                                                        <div class="mt-2" id="playlits-times-'.$i.'">
                                                            <!--p class="tt-4 ms-5 ps-4">Workout Time <span class="text-light">'.$play['w_time'].' S</span></p-->
                                                            <span class="tt-4 ms-5 ps-4">Workout Time <span class="text-light" style="margin-left:19px">'.str_pad($play['w_h'],2,"0",STR_PAD_LEFT).' : '.str_pad($play['w_m'],2,"0",STR_PAD_LEFT).' : '.str_pad($play['w_s'],2,"0",STR_PAD_LEFT).'</span></span>
                                                            <!--p class="tt-4 ms-4">Rest Time <span class="text-light">'.$play['r_time'].' S</span></p-->
                                                            <span class="tt-4 ms-4">Rest Time <span class="text-light ms-2">'.str_pad($play['r_h'],2,"0",STR_PAD_LEFT).' : '.str_pad($play['r_m'],2,"0",STR_PAD_LEFT).' : '.str_pad($play['r_s'],2,"0",STR_PAD_LEFT).'</span></span>
                                                            <span class="tt-4 ms-4">Number of Rounds <span class="text-light ms-2">'.$play['rounds'].'</span></span>                                                            
                                                            <br/>
                                                            <div class="ms-5 mt-1">
                                                            <!--p class="tt-4 ms-4">Hydration Break <span class="text-light">'.$play['hbr'].' S</span></p--> 
                                                            <span class="tt-4 ms-4">Hydration Break <span class="text-light" style="margin-left:5px">'.str_pad($play['hb_h'],2,"0",STR_PAD_LEFT).' : '.str_pad($play['hb_m'],2,"0",STR_PAD_LEFT).' : '.str_pad($play['hb_s'],2,"0",STR_PAD_LEFT).'</span></span>
                                                            <span class="tt-4 ms-4"><span class="text-light">'.$eofsData[(($play['hbc'] > 0)?$play['hbc'] - 1 : 0)].'</span></span>';
                                                            if($play['rounds'] > 1){
                                                                echo '<!--span class="tt-4 ms-4">End Of round rest <span class="text-light">'.$play['end_round_rest'].' S</span></span-->
                                                                <span class="tt-4 ms-4">End Of round rest <span class="text-light">'.str_pad($play['eor_h'],2,"0",STR_PAD_LEFT).' : '.str_pad($play['eor_m'],2,"0",STR_PAD_LEFT).' : '.str_pad($play['eor_s'],2,"0",STR_PAD_LEFT).'</span></span>';
                                                            }
                                                            echo '</div>                                                           
                                                        </div>
                                                        <div class="mt-2 d-none" id="playlits-times-edit-'.$i.'">
                                                            <span class="tt-6 ms-5 ps-4 me-4">Workout Time </span><input type="number" class="my-input-2" value="'.$play['w_h'].'" id="playlist-time-edit-wh-'.$i.'" /><span class="text-light"> : </span><input type="number" class="my-input-2" value="'.$play['w_m'].'" id="playlist-time-edit-wm-'.$i.'"/><span class="text-light"> : </span><input type="number" class="my-input-2" value="'.$play['w_s'].'" id="playlist-time-edit-ws-'.$i.'"/>
                                                            <span class="tt-6 ms-4 ps-4">Rest Time </span><input type="number" class="my-input-2" value="'.$play['r_h'].'" id="playlist-time-edit-rh-'.$i.'"/><span class="text-light"> : </span><input type="number" class="my-input-2" value="'.$play['r_m'].'" id="playlist-time-edit-rm-'.$i.'"/><span class="text-light"> : </span><input type="number" class="my-input-2" value="'.$play['r_s'].'" id="playlist-time-edit-rs-'.$i.'"/>
                                                            <span class="tt-6 ms-4 ps-4 me-2">Number of Rounds </span><input type="number" disabled class="my-input-2" value="'.$play['rounds'].'" id="playlist-nor-edit-'.$i.'"/>
                                                            <span class="round-up-holder"><img class="round-end-ico-try-1" data-id="'.$i.'" src="img/polygon-1.png"/><img class="round-end-ico-try-2" data-id="'.$i.'" src="img/polygon-2.png"/></span>
                                                            <br/><div class="mt-2 d-flex">
                                                            <span class="tt-6 ms-5 ps-4" style="margin-right:15px;margin-top:10px;">Hydration Break </span><input type="number" class="my-input-2" value="'.$play['hb_h'].'" id="playlist-time-edit-hb_h-'.$i.'" /><span class="text-light" style="margin-top:6px;margin-left:3.5px;margin-right:3.5px"> : </span><input type="number" class="my-input-2" value="'.$play['hb_m'].'" id="playlist-time-edit-hb_m-'.$i.'"/><span class="text-light" style="margin-top:6px;margin-left:5px;margin-right:5px"> : </span><input type="number" class="my-input-2" value="'.$play['hb_s'].'" id="playlist-time-edit-hb_s-'.$i.'"/>
                                                            <div style="min-width:190px;background:#292929;border-radius:3px" class="ms-2 pt-1">
                                                                <select class="chosen-custom" id="playlist-end-of-slot-'.$i.'">';                                                                    
                                                                    foreach ($eofsData as $key => $val) {
                                                                        if(($key+1) == $play['hbc']){
                                                                            echo '<option value="'.($key+1).'" selected>'.$val.'</option>';
                                                                        }else{
                                                                            echo '<option value="'.($key+1).'">'.$val.'</option>';
                                                                        }                                                                        
                                                                    }
                                                                echo '</select>
                                                            </div>';
                                                            if($play['rounds'] > 1){
                                                                echo '<div class="animate__animated animate__faster animate__fadeIn" id="end_of_round_rest_holder-'.$i.'">
                                                                <span class="tt-6 ms-4 ps-4" style="margin-right:15px;margin-top:10px;">End Of round rest </span><input type="number" class="my-input-2" value="'.$play['eor_h'].'" id="playlist-time-edit-eor_h-'.$i.'"/><span class="text-light" style="margin-top:6px;margin-left:0px;margin-right:0px"> : </span><input type="number" class="my-input-2" value="'.$play['eor_m'].'" id="playlist-time-edit-eor_m-'.$i.'"/><span class="text-light" style="margin-top:6px;margin-left:0px;margin-right:0px"> : </span><input type="number" class="my-input-2" value="'.$play['eor_s'].'" id="playlist-time-edit-eor_s-'.$i.'"/>
                                                            </div>';
                                                            }else{
                                                                echo '<div class="d-none animate__animated animate__faster animate__fadeIn" id="end_of_round_rest_holder-'.$i.'">
                                                                <span class="tt-6 ms-4 ps-4" style="margin-right:15px;margin-top:10px;">End Of round rest </span><input type="number" class="my-input-2" value="'.$play['eor_h'].'" id="playlist-time-edit-eor_h-'.$i.'"/><span class="text-light" style="margin-top:6px;margin-left:0px;margin-right:0px"> : </span><input type="number" class="my-input-2" value="'.$play['eor_m'].'" id="playlist-time-edit-eor_m-'.$i.'"/><span class="text-light" style="margin-top:6px;margin-left:0px;margin-right:0px"> : </span><input type="number" class="my-input-2" value="'.$play['eor_s'].'" id="playlist-time-edit-eor_s-'.$i.'"/>
                                                            </div>';
                                                            }
                                                            echo '</div>
                                                        </div>                                                
                                                    </div>
                                                </div>
                                                <span class="playlist-edit-ico d-none animate__animated animate__faster playlist-edit-click" data-bt="1" data-my="'.$i.'" id="target-ex-2-'.$i.'">
                                                    <img src="img/pencil.png" class="pencil-ico"/>
                                                </span>                                        
                                            </div>
                                            <div class="collapse" id="collapseExample-'.$i.'">
                                                <div class="" id="playlits-content-1-'.$i.'">
                                                    <div class="playlist-holder-2 do-nicescrol-5">';
                                                        $video2 = new Video($db);
                                                        $vcount1 = 1;
                                                        try{
                                                            foreach($video2->playListVideo2($i) as $vKey => $vid){
                                                                echo '<div class="playlist-holder-3">
                                                                <div class="playlist-holder-4">
                                                                    <p class="tt-5 pt-2 ps-3">0'.$vcount1.'</p>                                                            
                                                                </div>
                                                                <div class="playlist-holder-5" style="border-radius: 5px;">
                                                                    <div class="card-2" data-vid="'.$vid['id'].'" style=" outline: none; 
                                                                    position: relative; width:100%;min-height:190px" tabindex="11">
                                                                        <div class="p-1">
                                                                            <video 
                                                                            id="my-video-play-'.$vid['id'].'"
                                                                            class="video-js"
                                                                            controls
                                                                            preload="auto"
                                                                            width="100%"
                                                                            height="128px"
                                                                            data-setup="{}" src="'.$vid['link'].'" data-color="#ffffff"></video>
                                                                            
                                                                        </div>
                                                                        <span class="line-1"></span>
                                                                        <span class="tt-3">'.$vid['name'].'</span>
                                                                        <span class="vid-ico-bg-2 vid-right-click" data-vid="'.$vid['id'].'">
                                                                            <!--i class="fa fa-ellipsis-h text-light" aria-hidden="true"></i-->
                                                                            <i class="fa fa-circle fa-sm text-light" style="font-size:5px;padding:0.66px;margin-top:2px" aria-hidden="true"></i>
                                                                            <i class="fa fa-circle fa-sm text-light" style="font-size:5px;padding:0.66px;margin-top:2px" aria-hidden="true"></i>
                                                                            <i class="fa fa-circle fa-sm text-light" style="font-size:5px;padding:0.66px;margin-top:2px" aria-hidden="true"></i>
                                                                        </span>                               
                                                                    </div>
                                                                </div>
                                                            </div>';
                                                            $vcount1 += 1;
                                                                
                                                            }
                                                        }catch(Exception $ex){}
                                                        for ($vcount1; $vcount1 < 9 ; $vcount1++) { 
                                                            echo '<div class="playlist-holder-3">
                                                                    <div class="playlist-holder-4">
                                                                        <p class="tt-5 pt-2 ps-3">0'.$vcount1.'</p>                                                            
                                                                    </div>
                                                                    <div class="playlist-holder-5">
                                                                        <span class="band-2"></span>
                                                                    </div>
                                                                </div>';
                                                        }
                                                        
                                                   echo ' 
                                                    </div>
                                                </div>
                                                <div class="d-none" id="playlits-content-2-'.$i.'">
                                                    <div class="playlist-holder-2 do-nicescrol-5">';
                                                        $vcount2 = 1;
                                                        try{
                                                            foreach($video2->playListVideo2($i) as $vKey => $vid){
                                                                echo '<div class="playlist-holder-3">
                                                                <div class="playlist-holder-4">
                                                                    <p class="tt-5 pt-2 ps-3">0'.$vcount2.'</p> 
                                                                    <span class="ico-remove-1" data-in="'.$i.'" data-de="'.$vcount2.'" id="vid-drop-rb-'.$vcount2.'-'.$i.'">
                                                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                                                    </span>                                                           
                                                                </div>
                                                                <div class="playlist-holder-6 dropable-test" data-vid="'.$vid['id'].'" data-in="'.$i.'" data-de="'.$vcount2.'" id="vid-drop-'.$vcount2.'-'.$i.'">
                                                                    <div class="card-2 do-nicescrol-2" data-vid="'.$vid['id'].'" style="overflow: hidden; outline: none; 
                                                                    position: relative; left: 7px; top: 5px;" tabindex="11">
                                                                        <div class="p-1">
                                                                            <video 
                                                                            id="my-video-play-edit-'.$vid['id'].'"
                                                                            class="video-js"
                                                                            controls
                                                                            preload="auto"
                                                                            width="100%"
                                                                            height="128px"
                                                                            data-setup="{}" src="'.$vid['link'].'" data-color="#ffffff"></video>
                                                                        </div>
                                                                        <span class="line-1"></span>
                                                                        <span class="tt-3">'.$vid['name'].'</span>
                                                                        <span class="vid-ico-bg-2 vid-right-click" data-vid="'.$vid['id'].'">
                                                                            <!--i class="fa fa-ellipsis-h text-light" aria-hidden="true"></i-->
                                                                            <i class="fa fa-circle fa-sm text-light" style="font-size:5px;padding:0.66px;margin-top:2px" aria-hidden="true"></i>
                                                                            <i class="fa fa-circle fa-sm text-light" style="font-size:5px;padding:0.66px;margin-top:2px" aria-hidden="true"></i>
                                                                            <i class="fa fa-circle fa-sm text-light" style="font-size:5px;padding:0.66px;margin-top:2px" aria-hidden="true"></i>
                                                                        </span>                               
                                                                    </div>
                                                                </div>
                                                            </div>';
                                                            $vcount2 += 1;
                                                                
                                                            }
                                                        }catch(Exception $ex){}
                                                        for ($vcount2; $vcount2 < 9 ; $vcount2++) { 
                                                           echo '<div class="playlist-holder-3">
                                                                <div class="playlist-holder-4">
                                                                    <p class="tt-5 pt-2 ps-3">0'.$vcount2.'</p>  
                                                                    <span class="ico-remove-1 d-none" data-in="'.$i.'" data-de="'.$vcount2.'" id="vid-drop-rb-'.$vcount2.'-'.$i.'">
                                                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                                                    </span>                                                           
                                                                </div>
                                                                <div class="playlist-holder-6 dropable-test" data-vid="null" data-in="'.$i.'" data-de="'.$vcount2.'" id="vid-drop-'.$vcount2.'-'.$i.'">
                                                                    <span class="ico-box-2"></span>
                                                                    <span class="ico-box-1"></span>
                                                                </div>
                                                            </div>';
                                                        }
                                                        
                                                     echo' 
                                                    </div>
                                                    <div class="d-flex justify-content-between">
                                                        <span class="ico-delete-1 platlist-delete-btn" data-pid="'.$i.'">
                                                            <i class="del"></i>
                                                        </span>
                                                        <div class="playlist-update-btn-holder">
                                                            <button type="button" class="mybtn-3 save-playlist-cancel" data-bt="2" data-my="'.$i.'" id="target-ex-2-'.$i.'">Cancel</button>
                                                            <button type="button" class="mybtn-2 save-playlist-update" data-pid="'.$i.'">Save</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                                ';
                            }
                        }catch(Exception $ex){}
                        ?>
                    </div>
                </div>                 
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="edit-video-title-model" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content bg-dark-0">
                <div class="modal-header bg-green-0 border-0">
                    
                <h5 class="modal-title text-light" id="exampleModalLabel">Edit video title</h5>
                <button type="button" class="btn-close-cust" data-bs-dismiss="modal" aria-label="Close">
                    <svg viewbox="0 0 40 40">
                        <path class="close-x" d="M 10,10 L 30,30 M 30,10 L 10,30" />
                    </svg>
                </button>
                </div>
                <form method="POST" action="action/video-title-update.php" id="video-title-update-form" enctype="multipart/form-data">
                    <div class="modal-body">
                    <div class="container">                        
                            <div class="row">
                                <input type="hidden" name="video-id" id="video-id-edit-1" class="my-input-3 mb-3">
                                <div class="col-12">
                                    <input type="text" name="video-title" id="video-title-edit-1" class="my-input-3 mb-3" placeholder="Exercise name">
                                </div>
                                                                                         
                            </div>                            
                    </div>
                    </div>
                    <div class="modal-footer border-0">
                    <button type="submit" name="tnmaeSave" data-mcid="<?php echo $_GET['c'] ?>" id="video-title-update-saving-btn" class="mybtn-2 edit-vid-tiitle-save">Save</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="exercise-add-new-model" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content bg-dark-0">
                <div class="modal-header bg-green-0 border-0">
                    
                <h5 class="modal-title text-light" id="exampleModalLabel">Add a new exercise</h5>
                <button type="button" class="btn-close-cust" data-bs-dismiss="modal" aria-label="Close">
                    <svg viewbox="0 0 40 40">
                        <path class="close-x" d="M 10,10 L 30,30 M 30,10 L 10,30" />
                    </svg>
                </button>
                </div>
                <form method="POST" action="action/add-new-exer.php" id="add-new-ex-form" enctype="multipart/form-data">
                    <div class="modal-body">
                    <!--div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <select class="chosen form-select" style="background: rebeccapurple;">
                                    <option >Hot Dog, Fries and a Soda</option>
                                    <option>Burger, Shake and a Smile</option>
                                    <option selected>Sugar, Spice and all things nice</option>
                                </select>
                            </div>
                        </div>
                    </div-->
                    <div class="container">                        
                            <div class="row">
                                <div class="col-12">
                                    <input type="text" name="excer-name" id="up-new-excer-name-getaaw" class="my-input-3 mb-3" placeholder="Exercise name">
                                </div>
                                <div class="col-12 mt-4">    
                                    <input type="hidden" class="my-input-3" name="cid" value="<?php echo $_GET['c'] ?>" placeholder="Playlist name" required>                         
                                       <div id="add-new-excer-maincat-holder"> 
                                    <?php
                                        try{
                                            $video = new Video($db);
                                            foreach($video->getCategories() as $mCatkey => $mcat){
                                                if($mcat['id'] == $_GET['c']){
                                                    echo '<button type="button" value="'.$mcat['id'].'" class="mybtn-1 me-1 new-ex-add-mcat active-1">'.$mcat['name'].'</button>';  
                                                }else{
                                                    echo '<button type="button" value="'.$mcat['id'].'" class="mybtn-1 me-1 new-ex-add-mcat">'.$mcat['name'].'</button>';  
                                                } 
                                                                                             
                                            }
                                        }catch(Exception $ex){}
                                        ?></div>
                                    </div>
                                    <div class="col-12 mt-3">
                                        <div style="background:#292929;padding-top:3px;padding-bottom:5px;border-radius:3px">
                                        <select class="chosen form-select" placeholder="1" name="sub-cat" style="background: rebeccapurple;" id="add-new-ex-mcat-area">
                                            <option value="" disabled selected>Exercise type</option>
                                            <?php
                                            try{
                                                $video = new Video($db);
                                                foreach($video->getSubCategories($_GET['c']) as $sCatKey => $scat){                                                    
                                                    echo '<option value="'.$scat['id'].'">'.$scat['name'].'</option>';  
                                                                                                
                                                }
                                            }catch(Exception $ex){}
                                            ?>  
                                        </select>
                                        </div>
                                    </div>                       
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <p class="tt-7 pt-4 ms-4">Exercise video <br/><br/><span id="ecxer-vdname-tag"></span></p>
                                </div>
                                <div class="col-6 d-flex flex-row-reverse pt-4">
                                    <div class="playlist-icon-up" id="exervid-ico-upmenu">
                                        <img src="img/cloud-up.png" id="play-up-pimg1" class="cloud-up-ico"/>
                                        <!--img src="" id="play-up-pimg2" class="playlit-up-pimg1 d-none"/-->                                        
                                            
                                    </div>
                                    <input type="file"  name="excer-vid" id="excervid-ico-up" accept=".mp4, .mpeg" class="d-none">
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-12">
                                    <div class="progress bg-dark-2">
                                        <div class="progress-bar bg-green-0"></div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div id="uploadStatus"></div>
                                </div>
                            </div>
                            
                    </div>
                    </div>
                    <div class="modal-footer border-0">
                    <button type="submit" name="sCatMan" data-mcid="<?php echo $_GET['c'] ?>" id="new-video-saving-btn" class="mybtn-2 new-exerc-save">Save</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="exercise-type-model" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content bg-dark-0">
                <div class="modal-header bg-green-0 border-0">
                    
                <h5 class="modal-title text-light" id="exampleModalLabel">Exercise Type</h5>
                <button type="button" class="btn-close-cust" data-bs-dismiss="modal" aria-label="Close">
                    <svg viewbox="0 0 40 40">
                        <path class="close-x" d="M 10,10 L 30,30 M 30,10 L 10,30" />
                    </svg>
                </button>
                </div>
                <form method="POST" action="" enctype="multipart/form-data">
                    <div class="modal-body">
                    <!--div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <select class="chosen form-select" style="background: rebeccapurple;">
                                    <option >Hot Dog, Fries and a Soda</option>
                                    <option>Burger, Shake and a Smile</option>
                                    <option selected>Sugar, Spice and all things nice</option>
                                </select>
                            </div>
                        </div>
                    </div-->
                    <div class="container">                        
                            <div class="row">
                                <div class="col-12">    
                                    <input type="hidden" class="my-input-3" name="cid" value="<?php echo $_GET['c'] ?>" placeholder="Plalist name" required>                         
                                    <div id="exe-type-mcat-holder">
                                    <?php
                                        try{
                                            $video = new Video($db);
                                            foreach($video->getCategories() as $mCatkey => $mcat){
                                                if($mcat['id'] == $_GET['c']){
                                                    echo '<button type="button" data-cid="'.$mcat['id'].'" class="exetype-handler-action-1 mybtn-1 me-1 active-1">'.$mcat['name'].'</button>';
                                                }else{
                                                    echo '<button type="button" data-cid="'.$mcat['id'].'" class="exetype-handler-action-1 mybtn-1 me-1">'.$mcat['name'].'</button>';
                                                }                                                   
                                                                                             
                                            }
                                        }catch(Exception $ex){}
                                        ?></div>
                                    </div>                          
                            </div>
                            <div class="row mt-4">
                                <div class="col-12" id="contain-scat-elements">
                                    <?php
                                        try{
                                            $video = new Video($db);
                                            foreach($video->getSubCategories($_GET['c']) as $sCatKey => $scat){
                                                echo '<div class="d-flex mb-1" id="scat-manage-ele-'.$scat['id'].'">
                                                    <span class="equ-ico">=</span>
                                                    <input type="text" class="mcat-add-in-1 scat-man-in-chan" data-cid="'.$scat['id'].'" id="scat-man-in-'.$scat['id'].'" value="'.$scat['name'].'" />
                                                    <span class="minus-ico-1 scat-in-del" data-mood="old" data-inp="#scat-man-in-'.$scat['id'].'" data-cid="'.$scat['id'].'" data-ele="#scat-manage-ele-'.$scat['id'].'">
                                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                                    </span>    
                                                </div>';   
                                                                                             
                                            }
                                        }catch(Exception $ex){}
                                        ?>                                    
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-12">
                                    <span class="tt-10">Add new exercise type</span>
                                    <div class="d-flex mt-2">
                                        <input type="text" id="add-scat-in-plus" class="mcat-add-in-1 ms-3" />
                                        <span class="plu-ico-1 add-scat-in-plus">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </span> 
                                    </div>
                                </div>
                            </div>
                            
                    </div>
                    </div>
                    <div class="modal-footer border-0">
                    <button type="button" name="sCatMan" id="excersise-tpye-save-btn-1" data-mcid="<?php echo $_GET['c'] ?>" class="mybtn-2 subcat-manage-save">Save</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="exercise-areas-model" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content bg-dark-0">
                <div class="modal-header bg-green-0 border-0">
                    
                <h5 class="modal-title text-light" id="exampleModalLabel">Exercise Areas</h5>
                <button type="button" class="btn-close-cust" data-bs-dismiss="modal" aria-label="Close">
                    <svg viewbox="0 0 40 40">
                        <path class="close-x" d="M 10,10 L 30,30 M 30,10 L 10,30" />
                    </svg>
                </button>
                </div>
                <form method="POST" action="" enctype="multipart/form-data">
                    <div class="modal-body">
                    <!--div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <select class="chosen form-select" style="background: rebeccapurple;">
                                    <option >Hot Dog, Fries and a Soda</option>
                                    <option>Burger, Shake and a Smile</option>
                                    <option selected>Sugar, Spice and all things nice</option>
                                </select>
                            </div>
                        </div>
                    </div-->
                    <div class="container">                        
                            <div class="row">
                                <div class="col-12">    
                                    <input type="hidden" class="my-input-3" name="cid" value="<?php echo $_GET['c'] ?>" placeholder="Plalist name" required>                         
                                    <?php
                                        try{
                                            $video = new Video($db);
                                            foreach($video->getCategories() as $mCatkey => $mcat){
                                                echo '<button type="button" class="mybtn-1 me-1 active-1">'.$mcat['name'].'</button>';   
                                                                                             
                                            }
                                        }catch(Exception $ex){}
                                        ?>
                                    </div>                          
                            </div>
                            <div class="row mt-4">
                                <div class="col-12" id="contain-mcat-elements">
                                    <?php
                                        try{
                                            $video = new Video($db);
                                            foreach($video->getCategories() as $mCatkey => $mcat){
                                                echo '<div class="d-flex mb-1" id="maincat-manage-ele-'.$mcat['id'].'">
                                                    <span class="equ-ico">=</span>
                                                    <input type="text" class="mcat-add-in-1 mcat-man-in-chan" data-cid="'.$mcat['id'].'" id="maincat-man-in-'.$mcat['id'].'" value="'.$mcat['name'].'" />
                                                    <span class="minus-ico-1 mcat-in-del" data-mood="old" data-inp="#maincat-man-in-'.$mcat['id'].'" data-cid="'.$mcat['id'].'" data-ele="#maincat-manage-ele-'.$mcat['id'].'">
                                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                                    </span>    
                                                </div>';   
                                                                                             
                                            }
                                        }catch(Exception $ex){}
                                        ?>                                    
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-12">
                                    <span class="tt-10">Add new exercise area</span>
                                    <div class="d-flex mt-2">
                                        <input type="text" id="add-mcat-in-plus" class="mcat-add-in-1 ms-3" />
                                        <span class="plu-ico-1 add-mcat-in-plus">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </span> 
                                    </div>
                                </div>
                            </div>
                            
                    </div>
                    </div>
                    <div class="modal-footer border-0">
                    <button type="button" name="mainCatMan" class="mybtn-2 maincat-manage-save">Save</button>
                    </div>
                </form>
            </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="new-playlist" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content bg-dark-0">
                <div class="modal-header bg-green-0 border-0">
                    
                <h5 class="modal-title text-light" id="exampleModalLabel">Add a new playlist</h5>
                <button type="button" class="btn-close-cust" data-bs-dismiss="modal" aria-label="Close">
                    <svg viewbox="0 0 40 40">
                        <path class="close-x" d="M 10,10 L 30,30 M 30,10 L 10,30" />
                    </svg>
                </button>
                </div>
                <form method="POST" action="action/new-playlist.php" enctype="multipart/form-data">
                    <div class="modal-body">
                    <!--div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <select class="chosen form-select" style="background: rebeccapurple;">
                                    <option >Hot Dog, Fries and a Soda</option>
                                    <option>Burger, Shake and a Smile</option>
                                    <option selected>Sugar, Spice and all things nice</option>
                                </select>
                            </div>
                        </div>
                    </div-->
                    <div class="container">                        
                            <div class="row">  
                                <input type="hidden" class="my-input-3" name="cid" value="<?php echo $_GET['c'] ?>" placeholder="Playlist name" required>                         
                                <div class="col-6">
                                    <div class="row">
                                        <div class="col-12">
                                            <input type="text" name="name" class="my-input-3" placeholder="Playlist name" oninvalid="this.setCustomValidity('Please fill out the playlist name')" oninput="this.setCustomValidity('')" required>
                                        </div>
                                        <div class="col-6">
                                            <p class="tt-7 pt-4 ms-4">Workout time</p>
                                        </div>
                                        <div class="col-6 d-flex">
                                            <input type="number" name="wh" class="my-input-2 mt-3 ms-4" min="0" max="23" value="0" required/>                                                                     
                                            <span class="text-light pt-4 ps-1 pe-1">:</span>                            
                                            <input type="number" name="wm" class="my-input-2 mt-3" min="0" max="60" value="0" required/>                             
                                            <span class="text-light pt-4 ps-1 pe-1">:</span>                                
                                            <input type="number" name="ws" class="my-input-2 mt-3" min="0" max="60" value="0" required/>                                         
                                        </div>
                                        <div class="col-6">
                                            <p class="tt-7 pt-4 ms-4">Rest time</p>
                                        </div>
                                        <div class="col-6 d-flex">
                                            <input type="number" name="rh" class="my-input-2 mt-3 ms-4" min="0" max="23" value="0" required/>                                                                     
                                            <span class="text-light pt-4 ps-1 pe-1">:</span>                            
                                            <input type="number" name="rm" class="my-input-2 mt-3" min="0" max="60" value="0" required/>                             
                                            <span class="text-light pt-4 ps-1 pe-1">:</span>   
                                            <input type="number" name="rs" class="my-input-2 mt-3" min="0" max="60" value="0" required/>                             
                                                                                    
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="row">
                                        <div class="col-6">
                                            <p class="tt-7 ms-4">Playlist icon</p>
                                        </div>
                                        <div class="col-6 d-flex flex-row-reverse ">
                                            <div class="playlist-icon-up" id="plaulist-ico-upmenu">
                                                <img src="img/cloud-up.png" id="play-up-pimg1" class="cloud-up-ico"/>
                                                <img src="" id="play-up-pimg2" class="playlit-up-pimg1 d-none"/>
                                            </div>
                                            <input type="file" onchange="previewFile(this)" accept=".jpg,.jpeg,.png,.JPG,.JPEG,.PNG" name="playlist-ico-up" id="playlist-ico-up" class="d-none">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-6">
                                                    <p class="tt-7 pt-4 ms-4">Number of rounds</p>
                                                </div>
                                                <div class="col-6 d-flex">
                                                    <input type="number" name="nor" id="new_nor_val" class="my-input-2 mt-3 ms-4" min="1" value="1" readonly="readonly" required/>
                                                    <span class="round-up-holder-2"><img class="round-end-ico-try-3" data-id="" src="img/polygon-1.png"/><img class="round-end-ico-try-4" data-id="" src="img/polygon-2.png"/></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-6">
                                                    <p class="tt-7 pt-4 ms-4">End of round rest</p>
                                                </div>
                                                <div class="col-6 d-flex">
                                                    <input type="number" name="eor_h" class="my-input-2 mt-3 ms-4" min="0" max="23" value="0" required/>                                                                     
                                                    <span class="text-light pt-4 ps-1 pe-1">:</span>                            
                                                    <input type="number" name="eor_m" class="my-input-2 mt-3" min="0" max="60" value="0" required/>                             
                                                    <span class="text-light pt-4 ps-1 pe-1">:</span>   
                                                    <input type="number" name="eor_s" class="my-input-2 mt-3" min="0" max="60" value="0" required/> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-6">
                                                    <p class="tt-7 pt-4 ms-4">Hydration Break</p>
                                                </div>
                                                <div class="col-6 d-flex">
                                                    <input type="number" name="hb_h" class="my-input-2 mt-3 ms-4" min="0" max="23" value="0" required/>                                                                     
                                                    <span class="text-light pt-4 ps-1 pe-1">:</span>                            
                                                    <input type="number" name="hb_m" class="my-input-2 mt-3" min="0" max="60" value="0" required/>                             
                                                    <span class="text-light pt-4 ps-1 pe-1">:</span>   
                                                    <input type="number" name="hb_s" class="my-input-2 mt-3" min="0" max="60" value="0" required/> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-12 d-flex">
                                                    <div style="min-width:93%;background:#292929;border-radius:3px;height:39px;padding-top:2px" class="ms-4 mt-3">
                                                        <select class="chosen-custom" name="eos_val">
                                                            <?php                                                                   
                                                            foreach ($eofsData as $key => $val) {
                                                                echo '<option value="'.($key+1).'">'.$val.'</option>';                                                                     
                                                            }?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                                                                                
                                    
                            </div>
                            
                    </div>
                    </div>
                    <div class="modal-footer border-0">
                    <button type="submit" name="addNewPlaylist" class="mybtn-2">Save</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
        
    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>   
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
                                                             
    <script src="bootstrap-5.0.2-dist/js/bootstrap.min.js"></script>   
<script src="plugings/nicescroll/jquery.nicescroll.min.js"></script>
<!--script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js" integrity="sha512-zMfrMAZYAlNClPKjN+JMuslK/B6sPM09BGvrWlW+cymmPmsUT1xJF3P4kxI3lOh9zypakSgWaTpY6vDJY/3Dig==" crossorigin="anonymous" referrerpolicy="no-referrer"></script-->
<!--script src="plugings/skin-video-player/js/ckin.js"></script-->
<script src="https://use.fontawesome.com/e114048512.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>