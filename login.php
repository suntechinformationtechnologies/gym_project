<?php
session_start();
if(isset($_SESSION['login'])){
    if($_SESSION['login'] == "skjhgfdsjgfs7wrbfhjkmzdbt7wrto93"){
        header("Location:./");
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Gym Demo</title>   
    <link href="bootstrap-5.0.2-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/fonts.css" rel="stylesheet">
    <link href="css/layout.css" rel="stylesheet">
    <link href="css/icons.css" rel="stylesheet">
    <link href="css/colors.css" rel="stylesheet">
    <link href="css/buttons.css" rel="stylesheet">
    <link href="css/login.css" rel="stylesheet">
    <!--link href="css/style.css" rel="stylesheet"-->
    <!--link rel="stylesheet" href="plugings/skin-video-player/css/ckin.css"-->
    <link
  href="https://unpkg.com/video.js@7/dist/video-js.min.css"
  rel="stylesheet"
/>

<!-- City -->
<link
  href="https://unpkg.com/@videojs/themes@1/dist/city/index.css"
  rel="stylesheet"
/>

    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.js" integrity="sha512-eSeh0V+8U3qoxFnK3KgBsM69hrMOGMBy3CNxq/T4BArsSQJfKVsKb5joMqIPrNMjRQSTl4xG8oJRpgU2o9I7HQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css" integrity="sha512-yVvxUQV0QESBt1SyZbNJMAwyKvFTLMyXSyBHDO4BG5t7k/Lw34tyqlSDlKIrIENIzCl+RVUNjmCPG+V/GMesRw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="plugings/chosen/chosen-dark.css">
<link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        
        <div class="row justify-content-center">
            <div class="col-12 col-xl-5 col-lg-6 col-sm-6" style="margin-top: 70px;">
                <div class="row justify-content-cente">
                    <div class="col-12">
                        <div class="d-flex justify-content-center">
                            <span class="band-5"></span>                            
                        </div>
                        <div class="text-center mt-4 pt-2 mb-5">
                          <p class="lg-top-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus nisl, duis euismod ornare enim fermentum quis mattis.</p>
                        </div>                          
                    </div>
                    <form method="POST" action="action/login.php" id="login-form">
                      <div class="col-12">
                        <div class="d-flex justify-content-center">
                          <input type="text" name="un" value="<?php if(isset($_COOKIE["cfil-un"])) { echo $_COOKIE["cfil-un"]; } ?>" class="login-input" placeholder="Username" oninvalid="this.setCustomValidity('Please fill out the username')" oninput="this.setCustomValidity('')" required/>                                                
                        </div>                      
                      </div>
                      <div class="col-12">
                        <div class="d-flex justify-content-center">
                          <input type="password" name="pw" value="<?php if(isset($_COOKIE["cfil-pw"])) { echo $_COOKIE["cfil-pw"]; } ?>" class="login-input" placeholder="Password" oninvalid="this.setCustomValidity('Please fill out the password')" oninput="this.setCustomValidity('')" required/>                                                
                        </div>                      
                      </div>
                      <div class="col-12">                    
                          <label class="container-1">
                            <input type="checkbox" name="rem" checked>
                            <span class="checkmark"></span><span class="ms-4 ps-2">Remember me</span>
                          </label>                                                                                           
                      </div>
                      <div class="col-12 mt-5">
                        <div class="d-flex justify-content-center">
                          <button type="submit" class="login-btn" id="login-btn">Login</button>                                               
                        </div>                      
                      </div>
                    </form>
                    <div class="col-12 mt-4">
                      <div class="text-center">
                        <p class="lg-top-text">Forgot your password ?</p>
                      </div>                   
                    </div>
                    <div class="col-12">
                      <div class="d-flex justify-content-center">
                        <span class="line-login"></span>                                               
                      </div>                      
                    </div>
                    <div class="col-12 mt-5 pt-3">
                      <div class="text-center">
                        <p class="lg-top-text">Product version v1.1.1</p>
                      </div>                   
                    </div>
                </div>
            </div>
          </div></div>
    </div>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="bootstrap-5.0.2-dist/js/bootstrap.min.js"></script>   
<script src="plugings/nicescroll/jquery.nicescroll.min.js"></script>
<!--script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js" integrity="sha512-zMfrMAZYAlNClPKjN+JMuslK/B6sPM09BGvrWlW+cymmPmsUT1xJF3P4kxI3lOh9zypakSgWaTpY6vDJY/3Dig==" crossorigin="anonymous" referrerpolicy="no-referrer"></script-->
<!--script src="plugings/skin-video-player/js/ckin.js"></script-->
<script src="https://use.fontawesome.com/e114048512.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
<style>
  .swal-modal .swal-text {
    text-align: center;
}
</style>
<script>
  $("#login-form").on('submit',(function(e) {        
        $("#login-btn").html("wait...");
        e.preventDefault();
        $.ajax({
            url: "action/login.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
                if(data == "1"){
                  window.location.replace("");                                    
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Invalid Sign In',                        
                        html: "<span style='color:#F8BB86;'>The login details that you have entered did not match our records. Please double-check and try again.</span>",
                    });
                    $("#login-btn").html("Login");
                }
                
            }
        });        
        $("#video-title-update-saving-btn").html("save");
    }));
</script>
</body>
</html>