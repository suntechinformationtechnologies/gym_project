$('.round-end-ico-try-1').click(function(){
    var nrid = $(this).attr("data-id");
    var nrval = parseInt($("#playlist-nor-edit-"+nrid).val());
    var nrvalt = nrval + 1;
    $("#playlist-nor-edit-"+nrid).val(nrvalt);
    endOfRoundRestDivHandller(nrvalt,nrid);
});
$('.round-end-ico-try-2').click(function(){
    var nrid = $(this).attr("data-id");
    var nrval = parseInt($("#playlist-nor-edit-"+nrid).val());
    var nrvalt = nrval - 1;
    if(nrval > 1){        
        $("#playlist-nor-edit-"+nrid).val(nrvalt);
    }
    endOfRoundRestDivHandller(nrvalt,nrid);
});
$('.round-end-ico-try-3').click(function(){
    var nrval = parseInt($("#new_nor_val").val());
    var nrvalt = nrval + 1;
    $("#new_nor_val").val(nrvalt);
});
$('.round-end-ico-try-4').click(function(){
    var nrval = parseInt($("#new_nor_val").val());
    var nrvalt = nrval - 1;
    if(nrval > 1){        
        $("#new_nor_val").val(nrvalt);
    }
});
function endOfRoundRestDivHandller(norval,nridd){
    if(norval > 1){
      //  $("#end_of_round_rest_holder-"+nridd).addClass("animate__fadeIn");        
        $("#end_of_round_rest_holder-"+nridd).removeClass("d-none");
    }else{                        
      //  $("#end_of_round_rest_holder-"+nridd).removeClass("animate__fadeIn");
        $("#end_of_round_rest_holder-"+nridd).addClass("d-none");
    }
}
$(".platlist-delete-btn").click(function(){
    var pid = $(this).attr("data-pid");
    Swal.fire({
        title: 'Are you sure?',
        html: "<span style='color:#F8BB86'>This playlist will be removed permanently.</span>",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#0E7271',
        confirmButtonText: 'Remove'
    }).then((result) => {
    if (result.isConfirmed) {
        $.post( "action/delete-playlist.php", { 
            "pid" : pid,
        }).done(function( data ) {
            if(data == 1){
                Swal.fire(
                    'Playlist Deleted!',
                    '',
                    'success'
                ).then((result) => {
                    location.reload();
                });
                
                                    
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    html: "<span style='color:#F8BB86'>"+data+"</span>",
                });
            }
        }).fail(function() {
            alert( "error" );
        });
        }
    });
});
function loadTopviconSearch(cid,tag){
    $.post( "action/get-vid-2.php", { 
        "cid" : cid,
        "tag" : tag
    }).done(function( data ) {
        $("#video-content-top-1").html(data);
        $(".do-nicescrol-2").niceScroll({
            cursorcolor:"rgba(255,255,255,0.2)",
            background:"rgba(20,20,20,0)",
            cursorborder:"0.5px solid rgba(255,255,255,0)",
            cursorborderradius:0,            
        });
        $(".drag-test").draggable({
        helper: function (e, ui) {
            return $(this).clone();
        },
        appendTo: "body",
        /*stop: function (e, ui) {
            $('#drag-test').draggable().data()["ui-draggable"].cancelHelperRemoval = true;
        }*/
        });
    }).fail(function() {
        alert( "error" );
    });
}
//--------------- search ---------------------
//serach-in-text
//search-btn-ele
//search-can-btn
$('#search-can-btn').click(function(){
    let searchParams = new URLSearchParams(window.location.search);
    loadTopvicon(searchParams.get('c'));
    $("#search-btn-ele").removeClass("d-none");
    $("#search-can-btn").addClass("d-none");
    $('#serach-in-text').val("");
});
$('#search-btn-ele').click(function(){
    let searchParams = new URLSearchParams(window.location.search);
    var tag = $('#serach-in-text').val();
    var len = $('#serach-in-text').val().length;
    if(len > 0){
        loadTopviconSearch(searchParams.get('c'),tag);
        $("#search-btn-ele").addClass("d-none");
        $("#search-can-btn").removeClass("d-none");
    }else{
        loadTopvicon(searchParams.get('c'));
    }
});
/*$( "#serach-in-text" ).keyup(function() {
    var len = $(this).val().length;
    var tag = $(this).val();
    if(len > 0){
        $("#search-btn-ele").addClass("d-none");
        $("#search-can-btn").removeClass("d-none");
    }else{
        $("#search-btn-ele").removeClass("d-none");
        $("#search-can-btn").addClass("d-none");
    }
    
});*/
//--------------------------------
$("#menu-dele-vid").click(function(){
    var vid = $(this).attr("data-vid");
    var src = $(this).attr("data-src");
    let searchParams = new URLSearchParams(window.location.search);
    $("#menu-1").addClass("d-none");        
    Swal.fire({
        title: 'Are you sure?',
        html:`<span style='color:#F8BB86;'>This uploaded exercise video will be removed permanently.</span><br/>
        <div style='margin-left:90px' class="mt-4 card-2 do-nicescrol-2 drag-test" data-vid="'.$vid['id'].'">
                       <div class="p-1">
                           <video 
                                id="my-video-top-'.$vid['id'].'"
                                class="video-js"
                                controls
                                preload="auto"
                                width="100%"
                                data-setup="{}" src="`+src+`" data-color="#ffffff"></video>
                       </div>
                       <span class="line-1"></span>
                       <span class="tt-3">`+$(this).attr("data-title")+`</span>                              
                   </div>`,                
        text: "All the videos under this excercise type will be deleted!",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#0E7271',
        confirmButtonText: 'Delete'
    }).then((result) => {
    if (result.isConfirmed) {
        $.post( "action/delete-video.php", { 
            "vid" : vid,
            "src" : src
        }).done(function( data ) {
            if(data == 1){
                Swal.fire(
                    'Video Deleted!',
                    '',
                    'success'
                );                    
                loadTopvicon(searchParams.get('c'));
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    html: "<span style='color:#F8BB86'>"+data+"</span>",
                })
                loadTopvicon(searchParams.get('c'));
            }
        }).fail(function() {
            alert( "error" );
        });
        }
    });
});
$("#video-title-update-form").on('submit',(function(e) {
    var thisatr = this;
    $("#video-title-update-saving-btn").html("saving...");
    $("#video-title-update-saving-btn").prop("disabled",true);
    e.preventDefault();
    $.ajax({
        url: "action/video-title-update.php",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        success: function(data){
            if(data == "1"){
                Swal.fire({
                    icon: 'success',
                    title: 'Video Title Updated Successfully',                        
                    html: "<span style='color:#F8BB86;'>You have successfully updated the title of this video.</span>",
                }).then((result) => {
                    if (result.isConfirmed) {
                        $('#edit-video-title-model').modal('toggle');
                        }
                    });                   
                loadTopvicon($("#video-title-update-saving-btn").attr("data-mcid"));
                $("#video-title-update-saving-btn").html("save");
                $("#video-title-update-saving-btn").prop("disabled",false);
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    html: "<span style='color:#F8BB86'>"+data+"</span>",
                });
                $("#video-title-update-saving-btn").html("save");
                $("#video-title-update-saving-btn").prop("disabled",false);
            }
            
        }
    });        
}));
function loadSubCatDrop(cid){
    $.post( "action/load-subcats.php", {
        "c" :  cid
    }).done(function( data ) {
        $("#add-new-ex-mcat-area").html(data);
        $('#add-new-ex-mcat-area').trigger("chosen:updated");
    }).fail(function() {
        alert( "error" );
    });
}    
$(document).on("click",".new-ex-add-mcat",function(){
    var cid = $(this).val();
    $(".new-ex-add-mcat").removeClass("active-1");
    $(this).addClass("active-1");        
    loadSubCatDrop(cid);
    $("#new-video-saving-btn").attr("data-mcid",cid);
    
    
});
$(document).on("click",".open-new-excer-model-by-sub",function(){
    var scat = $(this).attr("data-sid");
   // console.log(scat);
    $("#add-new-ex-mcat-area").val(scat).change();
    $('#add-new-ex-mcat-area').trigger("chosen:updated");
    
});
$("#add-new-ex-form").on('submit',(function(e) {
    e.preventDefault();
    if(!$("#up-new-excer-name-getaaw").val()){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: "<span style='color:#F8BB86'>Please fill out the exercise name.</span>",
        });  
    }else if(!$("#add-new-ex-mcat-area").val()){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: "<span style='color:#F8BB86'>Please select a exercise type!</span>",
        });  
    }else if(!$("#excervid-ico-up").val()){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: "<span style='color:#F8BB86'>Please select a video file to upload.</span>",
        });  
    }else{
        var thisatr = this;
        $("#new-video-saving-btn").html("saving...");
        $("#new-video-saving-btn").prop("disabled",true);        
        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = parseFloat(((evt.loaded / evt.total) * 100)).toFixed(0);
                        $(".progress-bar").width(percentComplete + '%');
                        $(".progress-bar").html(percentComplete+'%');
                    }
                }, false);
                return xhr;
            },
            url: "action/add-new-exer.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $(".progress-bar").width('0%');
                $(".progress-bar").html('0%');
                //$('#uploadStatus').html('<img src="images/loading.gif"/>');
            },
            error:function(){
                $('#uploadStatus').html('<p style="color:#EA4335;">File upload failed, please try again.</p>');
            },
            success: function(data){
                if(data == "1"){
                    $('#add-new-ex-form')[0].reset();
                    //$('#uploadStatus').html('<p style="color:#28A74B;">File has uploaded successfully!</p>');
                    Swal.fire({
                        icon: 'success',
                        title: 'Exercise Video Added Successfully',                        
                        html: "<span style='color:#a6ffa9;'>You have successfully added a exercise video. This video now can be found under selected exercise area & exercise type.</span>",                        
                    }).then((result) => {
                        if (result.isConfirmed) {
                                $(".progress-bar").width('0%');
                                $(".progress-bar").html('0%');
                                $('#exercise-add-new-model').modal('toggle');
                            }
                    });
                    thisatr.reset();
                    $("#ecxer-vdname-tag").html("");
                    loadTopvicon($("#new-video-saving-btn").attr("data-mcid"));
                    handleMcatRout($("#new-video-saving-btn").attr("data-mcid"));
                    $("#new-video-saving-btn").html("save");
                    $("#new-video-saving-btn").prop("disabled",false);
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        html: "<span style='color:#F8BB86'>"+data+"</span>",
                    }).then((result) => {
                        if (result.isConfirmed) {
                                $(".progress-bar").width('0%');
                                $(".progress-bar").html('0%');
                            }
                    });
                    $("#new-video-saving-btn").html("save");
                    $("#new-video-saving-btn").prop("disabled",false);
                }
                
            }
        });}    
}));
 
$("#exervid-ico-upmenu").click(function(){
    //console.log(1);
    $("#excervid-ico-up").click();
});
$("#excervid-ico-up").change(function(){
    var fsiz = (this.files[0].size)/ 1048576;
    var fexten = $(this).val().replace(/C:\\fakepath\\/i, '').split('.').pop().toLowerCase();
    if(fsiz <= 100){
        if(jQuery.inArray(fexten,['mp4','mpeg']) == -1){
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                html: "<span style='color:#F8BB86'>File format is not supported ! <br/>(Supported formats : mp4, mpeg)</span>",
            });
            $(this).val("");
            $("#ecxer-vdname-tag").html("");
        }else{
            $("#ecxer-vdname-tag").html("*"+$(this).val().replace(/C:\\fakepath\\/i, '')+"<span class='ms-2 text-danger' id='video-input-clearer-1'>X</span>");
        }
    }else{        
        Swal.fire({
            icon: 'error',
            title: 'File Size Exceeds',
            html: "<span style='color:#F8BB86'>The uploaded video file likely exceeds the maximum file size (100 MB) that this server supports.</span>",
        });
        $(this).val("");
        $("#ecxer-vdname-tag").html("");
    }
});
/*$(".new-exerc-save").click(function(){
    //console.log($("#add-new-ex-mcat-area").val());
    
});

*/
$(document).on("click","#video-input-clearer-1",function(){
    $("#excervid-ico-up").val("");
    $("#ecxer-vdname-tag").html("");
    
});
//------------------------------------- manage exercice types ------------------------------------

var scatNew = [];
var scatDelSet = [];
var scatUpdSet = {};
var scatvar = 1;
$('.add-scat-in-plus').click(function(){
    if($('#add-scat-in-plus').val() != ""){
        var eleq2= `<div class="d-flex mb-1" id="scat-manage-ele-n`+scatvar+`">
                    <span class="equ-ico">=</span>
                    <input type="text" class="mcat-add-in-1" id="scat-man-in-n`+scatvar+`" value="`+$('#add-scat-in-plus').val()+`" />
                    <span class="minus-ico-1 scat-in-del" data-mood="new" data-cid="n`+scatvar+`" data-inp="#scat-man-in-n`+scatvar+`" data-ele="#scat-manage-ele-n`+scatvar+`">
                        <i class="fa fa-minus" aria-hidden="true"></i>
                    </span>    
                </div>`;
        $("#contain-scat-elements").append(eleq2);
        scatNew.push("#scat-man-in-n"+scatvar); 
        $('#add-scat-in-plus').val("");
        scatvar = scatvar + 1;
    }              
});
$("#exercise-type-model").on("hidden.bs.modal", function () {
    let searchParams = new URLSearchParams(window.location.search);
    //loadTopvicon();
    loadSubcats(searchParams.get('c'));
    scatNew = [];
    scatDelSet = [];
    scatUpdSet = {};
});
/*$(".scat-man-in-chan").change(function(){
    scatUpdSet[$(this).attr("data-cid")] = $(this).val();
});*/
$(document).on("change",".scat-man-in-chan",function(){
    scatUpdSet[$(this).attr("data-cid")] = $(this).val();        
});
function arrayRemove(arr, value) { 

    return arr.filter(function(ele){ 
        return ele != value; 
    });
}
$(document).on("click",".scat-in-del",function(){
    if($(this).attr("data-mood") == "new"){
        scatNew = arrayRemove(scatNew,$(this).attr("data-inp"));
        $($(this).attr("data-ele")).remove();
    }else if($(this).attr("data-mood") == "old"){            
        Swal.fire({
            title: 'Are you sure?',
            html: "<span style='color:#F8BB86;'>All the uploaded exercise videos under this exercise type category will be removed permanently.</span>",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Remove'
        }).then((result) => {
        if (result.isConfirmed) {
            scatDelSet.push($(this).attr("data-cid"));
            $($(this).attr("data-ele")).remove();
            }
        });
        
    }
    
});
$(document).on("click",".exetype-handler-action-1",function(){
    var cid = $(this).attr("data-cid");
    loadSubcats(cid);
    loadMcat(cid,1);       
    $("#excersise-tpye-save-btn-1").attr("data-mcid",cid);
    scatNew = [];
    scatDelSet = [];
    scatUpdSet = {};        
});
$(".subcat-manage-save").click(function(){
    var thisat = this;
    $(thisat).html("saving.."); 
    $(thisat).prop("disabled",false);
    //let searchParams = new URLSearchParams(window.location.search);
    //console.log(searchParams.get('c'));
    //console.log(scatDelSet);
    //console.log(scatUpdSet);
    //var mcid = searchParams.get('c');
    var mcid = $(thisat).attr("data-mcid");
    var sNew = [];
    for (var i = 0; i < scatNew.length; ++i) {
        sNew.push($(scatNew[i]).val());
    }
    $.post( "action/manage-subcat.php", {
        "mid" :  mcid,
        "new" : JSON.stringify(sNew),
        "update" : JSON.stringify(scatUpdSet),
        "delete" : JSON.stringify(scatDelSet),
    }).done(function( data ) {
        //console.log(data);
        let searchParams = new URLSearchParams(window.location.search);
        loadTopvicon(searchParams.get('c'));
        Swal.fire({
            icon: 'success',
            title: 'Exercise Type Updated Successfully',                        
            html: "<span style='color:#F8BB86;'>You have successfully updated the exercise type categories.</span>",
        }).then((result) => {
            if (result.isConfirmed) {
                $('#exercise-type-model').modal('toggle');
                loadTopvicon($(thisat).attr("data-mcid"));
                handleMcatRout($(thisat).attr("data-mcid"));
                }
            });
        $(thisat).html("save");
        $(thisat).prop("disabled",false);
    }).fail(function() {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: "<span style='color:#F8BB86'>"+data+"</span>",
        })
        $(thisat).html("save");
        $(thisat).prop("disabled",false);
    });
});
// -----------------------------------  manage excercie areas ------------------------------------------
var mcatNew = [];
var mcatDelSet = [];
var mcatUpdSet = {};
var mcatvar = 1;
$('.add-mcat-in-plus').click(function(){
    if($('#add-mcat-in-plus').val() != ""){
        var eleq1= `<div class="d-flex mb-1" id="maincat-manage-ele-n`+mcatvar+`">
                    <span class="equ-ico">=</span>
                    <input type="text" class="mcat-add-in-1" id="maincat-man-in-n`+mcatvar+`" value="`+$('#add-mcat-in-plus').val()+`" />
                    <span class="minus-ico-1 mcat-in-del" data-mood="new" data-cid="n`+mcatvar+`" data-inp="#maincat-man-in-n`+mcatvar+`" data-ele="#maincat-manage-ele-n`+mcatvar+`">
                        <i class="fa fa-minus" aria-hidden="true"></i>
                    </span>    
                </div>`;
        $("#contain-mcat-elements").append(eleq1);
        mcatNew.push("#maincat-man-in-n"+mcatvar); 
        mcatvar = mcatvar + 1;
    }              
});
$("#exercise-areas-model").on("hidden.bs.modal", function () {
    location.reload();
});
$(".mcat-man-in-chan").change(function(){
    mcatUpdSet[$(this).attr("data-cid")] = $(this).val();
});
function arrayRemove(arr, value) { 

    return arr.filter(function(ele){ 
        return ele != value; 
    });
}
$(document).on("click",".mcat-in-del",function(){
    if($(this).attr("data-mood") == "new"){
        mcatNew = arrayRemove(mcatNew,$(this).attr("data-inp"));
        $($(this).attr("data-ele")).remove();
    }else if($(this).attr("data-mood") == "old"){
        Swal.fire({
            title: 'Are you sure?',
            html: "<span style='color:#F8BB86'>All the uploaded exercise videos & exercise type categories under this exercise area category will be removed permanently.</span>",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Remove'
        }).then((result) => {
        if (result.isConfirmed) {
            mcatDelSet.push($(this).attr("data-cid"));
            $($(this).attr("data-ele")).remove();
            }
        });
        
    }
    
});

$(".maincat-manage-save").click(function(){
    var thisat = this;
    $(thisat).html("saving..");
    $(thisat).prop("disabled",true);
    //console.log(mcatDelSet);
    var mNew = [];
    for (var i = 0; i < mcatNew.length; ++i) {
        mNew.push($(mcatNew[i]).val());
    }        
    $.post( "action/manage-maincat.php", { 
        "new" : JSON.stringify(mNew),
        "update" : JSON.stringify(mcatUpdSet),
        "delete" : JSON.stringify(mcatDelSet),
    }).done(function( data ) {
        Swal.fire({
            icon: 'success',
            title: 'Exercise Area Updated Successfully',                        
            html: "<span style='color:#b8ffba;'>You have successfully updated the exercise area categories.</span>",   
        }).then((result) => {
            if (result.isConfirmed) {
                $('#exercise-areas-model').modal('toggle');
                }
            });
        $(thisat).html("save");
        $(thisat).prop("disabled",false);
        
    }).fail(function() {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: "<span style='color:#F8BB86'>"+data+"</span>",
        })
        $(thisat).html("save");
        $(thisat).prop("disabled",false);
    });
});

//------------------------------------- end of manage exercice areas -----------------------------


function previewFile(inputa){
    var fsizp = (inputa.files[0].size)/ 1048576;
    var fextenp = $(inputa).val().replace(/C:\\fakepath\\/i, '').split('.').pop().toLowerCase();
    if(fsizp <= 4){
        if(jQuery.inArray(fextenp,['jpg','jpeg','png']) == -1){
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                html: "<span style='color:#F8BB86'>File format is not supported ! <br/>(Supported formats : jpg, jpeg, png)</span>",
            });
            $(inputa).val("");
            $("#play-up-pimg2").addClass("d-none");
        }else{
            var file = $("#playlist-ico-up").get(0).files[0];

            if(file){
                var reader = new FileReader();

                reader.onload = function(){
                    //console.log(reader.result);
                    $("#play-up-pimg2").attr("src",reader.result);
                    $("#play-up-pimg2").removeClass("d-none");
                }

                reader.readAsDataURL(file);
            }
        }
    }else{
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: "<span style='color:#F8BB86'>File size should be less than 4 MB</span>",
        });
        $(inputa).val("");
        $("#play-up-pimg2").addClass("d-none");
    }    
}
$("#plaulist-ico-upmenu").click(function(){
    $("#playlist-ico-up").click();
});

$(".save-playlist-update").click(function(){
    var pid = $(this).attr("data-pid");
    
    var updatedPlayDataset = new FormData();
    var filesNewPlayIco = $('#playlist-ico-update-'+pid)[0].files[0];
    updatedPlayDataset.append('newPlayImg', filesNewPlayIco);
    updatedPlayDataset.append('pid' , pid);
    updatedPlayDataset.append('pname' , $("#playlist-name-edit-"+pid).val());
    updatedPlayDataset.append('pwh' , $("#playlist-time-edit-wh-"+pid).val());
    updatedPlayDataset.append('pwm' , $("#playlist-time-edit-wm-"+pid).val());
    updatedPlayDataset.append('pws' , $("#playlist-time-edit-ws-"+pid).val());
    updatedPlayDataset.append('prh' , $("#playlist-time-edit-rh-"+pid).val());
    updatedPlayDataset.append('prm' , $("#playlist-time-edit-rm-"+pid).val());
    updatedPlayDataset.append('prs' , $("#playlist-time-edit-rs-"+pid).val());
    updatedPlayDataset.append('nor' , $("#playlist-nor-edit-"+pid).val());
    updatedPlayDataset.append('hb_h' , $("#playlist-time-edit-hb_h-"+pid).val());
    updatedPlayDataset.append('hb_m' , $("#playlist-time-edit-hb_m-"+pid).val());
    updatedPlayDataset.append('hb_s' , $("#playlist-time-edit-hb_s-"+pid).val());
    updatedPlayDataset.append('eor_h' , $("#playlist-time-edit-eor_h-"+pid).val());
    updatedPlayDataset.append('eor_m' , $("#playlist-time-edit-eor_m-"+pid).val());
    updatedPlayDataset.append('eor_s' , $("#playlist-time-edit-eor_s-"+pid).val());
    updatedPlayDataset.append('eos' , $( "#playlist-end-of-slot-"+pid+" option:selected" ).val());

    updatedPlayDataset.append('oldimg' , $("#iconset-update-dataset-"+pid).attr("data-oldimg"));
    updatedPlayDataset.append('rem' , $("#iconset-update-dataset-"+pid).attr("data-rem"));
    updatedPlayDataset.append('up' , $("#iconset-update-dataset-"+pid).attr("data-up"));

    updatedPlayDataset.append('pv1' , $("#vid-drop-1-"+pid).attr("data-vid"));
    updatedPlayDataset.append('pv2' , $("#vid-drop-2-"+pid).attr("data-vid"));
    updatedPlayDataset.append('pv3' , $("#vid-drop-3-"+pid).attr("data-vid"));
    updatedPlayDataset.append('pv4' , $("#vid-drop-4-"+pid).attr("data-vid"));
    updatedPlayDataset.append('pv5' , $("#vid-drop-5-"+pid).attr("data-vid"));
    updatedPlayDataset.append('pv6' , $("#vid-drop-6-"+pid).attr("data-vid"));
    updatedPlayDataset.append('pv7' , $("#vid-drop-7-"+pid).attr("data-vid"));
    updatedPlayDataset.append('pv8' , $("#vid-drop-8-"+pid).attr("data-vid"));

    if($("#playlist-name-edit-"+pid).val().length > 0){
        $.ajax({
            url: 'action/manage-playlist.php',
            type: 'post',
            data: updatedPlayDataset,
            contentType: false,
            processData: false,
            success: function(response){
                if(response == "success"){
                    location.reload();
                // alert(response);
                }
                else{
                    alert('error: 5001');
                //alert(response);
                }
            },
        });
    }else{
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: "<span style='color:#F8BB86'>Please fill out the playlist name.</span>",
        });  
    }
   
    /*$.post( "action/manage-playlist.php", { 
        pid : pid,
        pname : $("#playlist-name-edit-"+pid).val(),
        pwh : $("#playlist-time-edit-wh-"+pid).val(),
        pwm : $("#playlist-time-edit-wm-"+pid).val(),
        pws : $("#playlist-time-edit-ws-"+pid).val(),
        prh : $("#playlist-time-edit-rh-"+pid).val(),
        prm : $("#playlist-time-edit-rm-"+pid).val(),
        prs : $("#playlist-time-edit-rs-"+pid).val(),
        nor : $("#playlist-nor-edit-"+pid).val(),
        hb_h : $("#playlist-time-edit-hb_h-"+pid).val(),
        hb_m : $("#playlist-time-edit-hb_m-"+pid).val(),
        hb_s : $("#playlist-time-edit-hb_s-"+pid).val(),
        eor_h : $("#playlist-time-edit-eor_h-"+pid).val(),
        eor_m : $("#playlist-time-edit-eor_m-"+pid).val(),
        eor_s : $("#playlist-time-edit-eor_s-"+pid).val(),
        eos : $( "#playlist-end-of-slot-"+pid+" option:selected" ).val(),

        oldimg : $("#iconset-update-dataset-"+pid).attr("data-oldimg"),
        rem : $("#iconset-update-dataset-"+pid).attr("data-rem"),
        up : $("#iconset-update-dataset-"+pid).attr("data-up"),

        pv1 : $("#vid-drop-1-"+pid).attr("data-vid"),
        pv2 : $("#vid-drop-2-"+pid).attr("data-vid"),
        pv3 : $("#vid-drop-3-"+pid).attr("data-vid"),
        pv4 : $("#vid-drop-4-"+pid).attr("data-vid"),
        pv5 : $("#vid-drop-5-"+pid).attr("data-vid"),
        pv6 : $("#vid-drop-6-"+pid).attr("data-vid"),
        pv7 : $("#vid-drop-7-"+pid).attr("data-vid"),
        pv8 : $("#vid-drop-8-"+pid).attr("data-vid")
    }).done(function( data ) {
        //alert( "Data Loaded: " + data );
        location.reload();
    }).fail(function() {
        alert( "error" );
    });*/
    

});
/*$.get( "action/get-vid-1.php", function( data ) {
    $( "#video-content-top-1" ).html( data );
});*/
function loadTopvicon(cid){
    $.post( "action/get-vid-1.php", { 
        "cid" : cid,
    }).done(function( data ) {
        $("#video-content-top-1").html(data);
        $(".do-nicescrol-2").niceScroll({
            cursorcolor:"rgba(255,255,255,0.2)",
            background:"rgba(20,20,20,0)",
            cursorborder:"0.5px solid rgba(255,255,255,0)",
            cursorborderradius:0,            
        });
        $(".drag-test").draggable({
        helper: function (e, ui) {
            return $(this).clone();
        },
        appendTo: "body",
        /*stop: function (e, ui) {
            $('#drag-test').draggable().data()["ui-draggable"].cancelHelperRemoval = true;
        }*/
        });
    }).fail(function() {
        alert( "error" );
    });
}
function loadSubcats(cid){
    $.post( "action/get-subcats.php", { 
        "c" : cid,
    }).done(function( data ) {
        $("#contain-scat-elements").html(data);            
    }).fail(function() {
        alert( "error" );
    });
}
function loadMcat(cid,t){
    $.post( "action/get-maincats.php", { 
        "c" : cid,
        "t" : t
    }).done(function( data ) {
        if(t == 1){
            $("#exe-type-mcat-holder").html(data);
        }else if(t == 2){
            $("#add-new-excer-maincat-holder").html(data);
        }                        
    }).fail(function() {
        alert( "error" );
    });
}
function handleMcatRout(cid){
    $(".mcat-change-btn").removeClass("active-1");
    $("#mcat-change-btn-"+cid).addClass("active-1");
    window.history.replaceState(null, null, "?c="+cid);
    loadTopvicon(cid);
    loadSubcats(cid);
    loadMcat(cid,1);
    loadMcat(cid,2);
    loadSubCatDrop(cid);
    $("#excersise-tpye-save-btn-1").attr("data-mcid",cid);
    $("#video-title-update-saving-btn").attr("data-mcid",cid);
}
$(".mcat-change-btn").click(function(){ 
    //window.location.replace("?c="+$(this).attr("data-cid"));
    var cid = $(this).attr("data-cid");
    handleMcatRout(cid);
    
});
$(document).on("click","#menu-edit-tit",function(){
    $("#menu-1").addClass("d-none");
    $("#video-id-edit-1").val($(this).attr("data-vid"));
    $("#video-title-edit-1").val($(this).attr("data-title"));
});   
$(document).on("click",".vid-right-click",function(){
    var vid = $(this).attr("data-vid");
    var tit = $(this).attr("data-title");
    var src = $(this).attr('data-src');        
    if($("#menu-1").attr("class") == "menu d-none" || $("#menu-edit-tit").attr("data-vid") != vid){
        $("#menu-1").removeClass("d-none");            

        $("#menu-add-play").attr("data-vid",vid);
        $("#menu-move-vid").attr("data-vid",vid);
        $("#menu-edit-tit").attr("data-vid",vid);
        $("#menu-edit-tit").attr("data-title",tit);
        $("#menu-dele-vid").attr("data-vid",vid);
        $("#menu-dele-vid").attr("data-title",tit);
        $("#menu-dele-vid").attr("data-src",src);

        var offs = $(this).last();
        var offset = offs.offset();
        $("#menu-1").css("top",(offset.top - 50) + 'px');
        $("#menu-1").css("left",(offset.left + 25) + 'px');
    }else{
        $("#menu-1").addClass("d-none");
    }
    
});
$(document).click((event) => {
    if (!$(event.target).closest('.vid-right-click').length) {
        if($("#menu-1").attr("class") != "menu d-none"){
            $("#menu-1").addClass("d-none");
        }
        
    }        
});
$(".playlist-edit-click, .save-playlist-cancel").click(function(){
    var rest = $(this).attr('data-my');
   if($(this).attr("data-bt") == 1){
       $(this).addClass("d-none");
   }else{
        $("#target-ex-2-"+rest).removeClass("d-none");
   }
    //console.log($("playcollap-"+$(this).attr("data-my")).attr("data-my"));        
    var ele = $("#playcollap-"+rest);
    if(ele.attr("data-toggle") == "collapse"){
        ele.attr("data-toggle","null");
        $("#playlits-content-2-"+rest).removeClass("d-none");
        $("#playlits-content-1-"+rest).addClass("d-none");
        $("#playlist-name-"+rest).addClass("d-none");
        $("#playlist-name-edit-"+rest).removeClass("d-none");
        $("#playlist-ico-edit-"+rest).removeClass("d-none");
        $("#playlits-times-"+rest).addClass("d-none");
        $("#playlits-times-edit-"+rest).removeClass("d-none");
        $("#target-img-1-"+rest).removeClass("d-flex animate__fadeIn");
        $("#target-img-1-"+rest).addClass("d-none animate__fadeOut");
    }else{
        ele.attr("data-toggle","collapse");
        $("#playlits-content-1-"+rest).removeClass("d-none");
        $("#playlits-content-2-"+rest).addClass("d-none");
        $("#playlist-name-edit-"+rest).addClass("d-none");
        $("#playlist-ico-edit-"+rest).addClass("d-none");
        $("#playlist-name-"+rest).removeClass("d-none");
        $("#playlits-times-edit-"+rest).addClass("d-none");
        $("#playlits-times-"+rest).removeClass("d-none");
        $("#target-img-1-"+rest).addClass("d-flex animate__fadeIn");
        $("#target-img-1-"+rest).removeClass("d-none animate__fadeOut");
    }
    $(".do-nicescrol-3").getNiceScroll().resize();   
    $(".do-nicescrol-5").getNiceScroll().resize();   
    $("body").getNiceScroll().resize();
});
$(document).on('click', '.off-collapsable', function(e) {
    e.stopPropagation();
    
   //$('#playcollap-'+$(this).attr("data-in")).preventDefault();.playlist-ico-remover
    
});
$(".playlist-ico-remover").click(function(){
    $("#target-img-ico-3-"+$(this).attr("my-data")).attr("src","source/thumbnails/default-9.png");
    $("#iconset-update-dataset-"+$(this).attr("my-data")).attr("data-rem","1");
    $("#iconset-update-dataset-"+$(this).attr("my-data")).attr("data-up","0");
});
$(".upd-playlist-ico-1").click(function(){
    $('#playlist-ico-update-'+$(this).attr("my-data"))[0].click();
});
function previewFileUpIco(imb){
   // console.log($(imb).attr("my-data"));
   var fsizp = (imb.files[0].size)/ 1048576;
    var fextenp = $(imb).val().replace(/C:\\fakepath\\/i, '').split('.').pop().toLowerCase();
    if(fsizp <= 4){
        if(jQuery.inArray(fextenp,['jpg','jpeg','png']) == -1){
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                html: "<span style='color:#F8BB86'>File format is not supported ! <br/>(Supported formats : jpg, jpeg, png)</span>",
            });
            $(imb).val("");
            //$("#play-up-pimg2").addClass("d-none");
        }else{
            var file = $('#playlist-ico-update-'+$(imb).attr("my-data")).get(0).files[0];

            if(file){
                var reader = new FileReader();

                reader.onload = function(){
                    //console.log(reader.result);
                    $("#target-img-ico-3-"+$(imb).attr("my-data")).attr("src",reader.result);
                    //$("#play-up-pimg2").removeClass("d-none");
                }
                $("#iconset-update-dataset-"+$(imb).attr("my-data")).attr("data-rem","1");
                $("#iconset-update-dataset-"+$(imb).attr("my-data")).attr("data-up","1");
                reader.readAsDataURL(file);
            }
        }
    }else{
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: "<span style='color:#F8BB86'>File size should be less than 4 MB</span>",
        });
        $(imb).val("");
        $("#play-up-pimg2").addClass("d-none");
    }    
}
$(".drag-test").draggable({
helper: function (e, ui) {
    return $(this).clone();
},
appendTo: "body",
/*stop: function (e, ui) {
    $('#drag-test').draggable().data()["ui-draggable"].cancelHelperRemoval = true;
}*/
});
$(".dropable-test").droppable({
    drop: function(event, ui) {
        var iid = $(this).attr("data-in");
        var vid = $(ui.draggable).attr("data-vid");
        var pv1 = $("#vid-drop-1-"+iid).attr("data-vid");
        var pv2 = $("#vid-drop-2-"+iid).attr("data-vid");
        var pv3 = $("#vid-drop-3-"+iid).attr("data-vid");
        var pv4 = $("#vid-drop-4-"+iid).attr("data-vid");
        var pv5 = $("#vid-drop-5-"+iid).attr("data-vid");
        var pv6 = $("#vid-drop-6-"+iid).attr("data-vid");
        var pv7 = $("#vid-drop-7-"+iid).attr("data-vid");
        var pv8 = $("#vid-drop-8-"+iid).attr("data-vid");
        if((vid == pv1) || (vid == pv2) || (vid == pv3) || (vid == pv4) || (vid == pv5) || (vid == pv6) || (vid == pv7) || (vid == pv8)){
            //alert("This video is already exist in this playlist");
            Swal.fire({
                icon: 'error',
                title: 'Video Cannot Be Added',                        
                html: "<span style='color:#F8BB86;'>This exercise video was added to this playlist previously & still exist on this playlist. Please try adding a diffrent exercise video.</span>",
            });
        }else{
            //console.log(this.id);
            //console.log($(ui.draggable).attr("data-vid"));
            $(this.id).html($(ui.draggable).draggable().data()["ui-draggable"]);
            $(this)
            .empty().append(ui.helper.clone(false).css({
                position: 'relative',
                left: '7px',
                top: '5px'
            }));
            $(this).attr("data-vid",$(ui.draggable).attr("data-vid"));
            $("#vid-drop-rb-"+$(this).attr("data-de")+"-"+$(this).attr("data-in")).removeClass("d-none");
        }
    }
});
$(".chosen").chosen({width: "100%"}); 
$('.ico-remove-1').click(function() {
   $(this).addClass("d-none");
   $("#vid-drop-"+$(this).attr("data-de")+"-"+$(this).attr("data-in")).html('<span class="ico-box-2"></span><span class="ico-box-1"></span>');
   $("#vid-drop-"+$(this).attr("data-de")+"-"+$(this).attr("data-in")).attr("data-vid","null");
});
$('.plalist-expand-dec').click(function() {
    var myClass = $(this).attr("data-my"); 
    $('#collapseExample-'+myClass).on('shown.bs.collapse', function () {
        $("#target-ex-1-"+myClass).addClass("d-flex animate__fadeIn");
        $("#target-ex-1-"+myClass).removeClass("d-none animate__fadeOut");
        $("#target-ex-2-"+myClass).removeClass("d-none animate__fadeOut"); 
        $("#target-ex-2-"+myClass).addClass("animate__fadeIn");          
        $(".do-nicescrol-3").getNiceScroll().resize();   
        $(".do-nicescrol-5").getNiceScroll().resize();   
        $("body").getNiceScroll().resize();   
       // console.log(1);
    });
    $('#collapseExample-'+myClass).on('hidden.bs.collapse', function () {
        $("#target-ex-1-"+myClass).addClass("animate__fadeOut");
        $("#target-ex-1-"+myClass).removeClass("animate__fadeIn");
        $("#target-ex-2-"+myClass).addClass("animate__fadeOut");
        $("#target-ex-2-"+myClass).removeClass("animate__fadeIn");
        $(".do-nicescrol-3").getNiceScroll().resize();
        $(".do-nicescrol-5").getNiceScroll().resize();
        $("body").getNiceScroll().resize(); 
       // console.log(2);
    });
    
});
$(".do-nicescrol-2").niceScroll({
    cursorcolor:"rgba(255,255,255,0.2)",
    background:"rgba(20,20,20,0)",
    cursorborder:"0.5px solid rgba(255,255,255,0)",
    cursorborderradius:0,
   /* emulatetouch: true,
    enableobserver: false*/
});
$(".do-nicescrol-3").niceScroll({
    cursorcolor:"rgba(255,255,255,0.2)",
    background:"rgba(20,20,20,0)",
    cursorborder:"0.5px solid rgba(255,255,255,0)",
    cursorborderradius:0,
    emulatetouch: true,
    enableobserver: true
});

$("body").niceScroll({
    cursorcolor:"rgba(14, 114, 113,0.3)",
    background:"rgba(14, 114, 113,0)",
    cursorborder:"1px solid rgba(14, 114, 113,0.3)",
    cursorborderradius:0,
    enableobserver: false
});
$(".do-nicescrol").niceScroll({
    cursorcolor:"#2b2b2b",
    background:"rgba(20,20,20,0)",
    cursorborder:"1px solid #2b2b2b",
    cursorborderradius:0,
    touchbehavior: true,
    
}); 
$(".do-nicescrol-5").niceScroll({
    cursorcolor:"rgba(20,20,20,0)",
    background:"rgba(20,20,20,0)",
    cursorborder:"1px solid rgba(20,20,20,0)",
    cursorborderradius:0,
    emulatetouch: true,     
    enableobserver: false  
}); 
$("body").scrollTop($("body")[0].scrollHeight);