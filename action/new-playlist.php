<?php
require_once "../inc/config.php";
require_once "../inc/FileUploader.php";
require_once "../inc/PlayList.php";
if(isset($_POST['addNewPlaylist'])){
    if(!isset($_FILES['playlist-ico-up']) || $_FILES['playlist-ico-up']['error'] == UPLOAD_ERR_NO_FILE) {
        $link = "";
    }else {
        try{
            $file = new FileUploader($_FILES['playlist-ico-up'],"image");
            $file->upload("source/thumbnails");
            $link = $file->getFileNameUploaded();         
        }catch(Exception $ex){
            //echo $ex->getMessage();
            header("location:../?c=".$_POST['cid']."&error=".$ex->getMessage());
        }    
    }
    $data_set = array(
        'date' => date("y-m-d h:i:s"), 
        'name' => $_POST['name'], 
        'wh' => $_POST['wh'], 
        'wm' => $_POST['wm'], 
        'ws' => $_POST['ws'], 
        'rh' => $_POST['rh'], 
        'rm' => $_POST['rm'], 
        'rs' => $_POST['rs'], 
        'thumb' => $link,
        "nor" => $_POST['nor'],
        "hb_h" => $_POST['hb_h'],
        "hb_m" => $_POST['hb_m'],
        "hb_s" => $_POST['hb_s'],
        "eor_h" => $_POST['eor_h'],
        "eor_m" => $_POST['eor_m'],
        "eor_s" => $_POST['eor_s'],
        'eos' => $_POST['eos_val']
    );
    try{
        $play = new PlayList($db);
        if($play->mewPlaylist($data_set)){
            header("location:../?c=".$_POST['cid']."&success=Playlist added!");
        }else{
            header("location:../?c=".$_POST['cid']."&error=Something went wrong!");
        }
    }catch(Exception $ex){
        //echo $ex->getMessage();
        header("location:../?c=".$_POST['cid']."&error=".$ex->getMessage());
    }
}
?>