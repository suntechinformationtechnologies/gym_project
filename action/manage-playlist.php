<?php
require_once "../inc/config.php";
require_once "../inc/FileUploader.php";
require_once "../inc/PlayList.php";

$imgUrl = basename($_POST['oldimg'],".d");
if($_POST['rem'] == "1"){
    if(basename($_POST['oldimg'],".d") != "default-9.png"){
        unlink("../source/thumbnails/".basename($_POST['oldimg'],".d"));
    }
    $imgUrl = ""; 
}else{
   $imgUrl = basename($_POST['oldimg'],".d");
}

if($_POST['up'] == "1"){
    try{
        $file = new FileUploader($_FILES['newPlayImg'],"image");
        $file->upload("source/thumbnails");
        $imgUrl = $file->getFileNameUploaded();         
    }catch(Exception $ex){
        $imgUrl = basename($_POST['oldimg'],".d");
    }  
}
$dataSet_1 = array(
    "id" => $_POST['pid'],
    "name" => $_POST['pname'],
    "w_h" => (strlen($_POST['pwh']) > 0)?$_POST['pwh']:0, 
    "w_m" => (strlen($_POST['pwm']) > 0)?$_POST['pwm']:0, 
    "w_s" => (strlen($_POST['pws']) > 0)?$_POST['pws']:0, 
    "r_h" => (strlen($_POST['prh']) > 0)?$_POST['prh']:0, 
    "r_m" => (strlen($_POST['prm']) > 0)?$_POST['prm']:0, 
    "r_s" => (strlen($_POST['prs']) > 0)?$_POST['prs']:0,
    "nor" => (strlen($_POST['nor']) > 0)?$_POST['nor']:1,
    "hb_h" => (strlen($_POST['hb_h']) > 0)?$_POST['hb_h']:0,
    "hb_m" => (strlen($_POST['hb_m']) > 0)?$_POST['hb_m']:0,
    "hb_s" => (strlen($_POST['hb_s']) > 0)?$_POST['hb_s']:0,
    "eor_h" => (strlen($_POST['eor_h']) > 0)?$_POST['eor_h']:0,
    "eor_m" => (strlen($_POST['eor_m']) > 0)?$_POST['eor_m']:0,
    "eor_s" => (strlen($_POST['eor_s']) > 0)?$_POST['eor_s']:0,
    'eos' => $_POST['eos'],
    'imgUrl' => $imgUrl,
);

try{
    $playlist = new PlayList($db);
    if($playlist->updatePlaylist($dataSet_1)){
        if($playlist->deletePlayVideo($_POST['pid'])){
            if($_POST['pv1'] != "null"){
                $playlist->addPlayVideo($_POST['pid'],$_POST['pv1']);
            }
            if($_POST['pv2'] != "null"){
                $playlist->addPlayVideo($_POST['pid'],$_POST['pv2']);
            }
            if($_POST['pv3'] != "null"){
                $playlist->addPlayVideo($_POST['pid'],$_POST['pv3']);
            }
            if($_POST['pv4'] != "null"){
                $playlist->addPlayVideo($_POST['pid'],$_POST['pv4']);
            }
            if($_POST['pv5'] != "null"){
                $playlist->addPlayVideo($_POST['pid'],$_POST['pv5']);
            }
            if($_POST['pv6'] != "null"){
                $playlist->addPlayVideo($_POST['pid'],$_POST['pv6']);
            }
            if($_POST['pv7'] != "null"){
                $playlist->addPlayVideo($_POST['pid'],$_POST['pv7']);
            }
            if($_POST['pv8'] != "null"){
                $playlist->addPlayVideo($_POST['pid'],$_POST['pv8']);
            }
            echo "success";
        }else{
            echo "Something went wrong!";
        }
    }else{  
        echo "Something went wrong!";
    }
}catch(PDOException $ex){
    print_r($ex->getCode());
}
?>