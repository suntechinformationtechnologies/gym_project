<?php
require_once "../inc/config.php";
require_once "../inc/PlayList.php";
try{
    $playlist = new PlayList($db);
    if($playlist->deletePlayVideo($_POST['pid'])){
        if($playlist->deletePlayList($_POST['pid'])){
            echo 1;
        }else{
            echo "Something went wrong!";
        }
    }else{
        echo "Something went wrong!";
    }            
}catch(Exception $ex){
    echo $ex->getMessage();
}
?>