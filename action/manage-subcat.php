<?php
require_once "../inc/config.php";
require_once "../inc/Video.php";
$cnew = json_decode($_POST['new']);
$cup = json_decode($_POST['update']);
$dup = json_decode($_POST['delete']);
try{
    $video = new Video($db);
    foreach($cup as $key => $val){
        try{
            $video->updateSubCat($key,$val);
        }catch(Exception $ex){
            echo $ex->getMessage();
        }
    }
}catch(Exception $ex){
    echo $ex->getMessage();
}
try{
    $video = new Video($db);
    foreach($cnew as $key => $val){
        try{
            $video->newSubCat($val,$_POST['mid']);
        }catch(Exception $ex){
            echo $ex->getMessage();
        }
    }
}catch(Exception $ex){
    echo $ex->getMessage();
}
try{
    $video = new Video($db);
    foreach($dup as $key => $val){   
        try{               
            foreach($video->getVidos($val) as $vKey => $vid){
                $video->deletePlayVideo($vid['id']);
                $video->deleteVideo($vid['id']);
                unlink("../source/videos/".$vid['link']);
            }
        }catch(Exception $ex){}
        $video->deleteSubCat($val);
    }
}catch(Exception $ex){
    echo $ex->getMessage();
}
?>