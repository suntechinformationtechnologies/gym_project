<?php
require_once "../inc/config.php";
require_once "../inc/Video.php";


try{
    $video = new Video($db);
    foreach($video->getSubCategories($_POST['cid']) as $sCatKey => $scat){
        echo '<div class="card-1">
                <p class="tt-2">'.$scat['name'].'</p>
                <span class="vid-ico-bg-1 open-new-excer-model-by-sub" data-sid="'.$scat['id'].'" data-bs-toggle="modal" data-bs-target="#exercise-add-new-model">
                    <i class="fa fa-plus vid-plus-1" aria-hidden="true"></i>
                </span>
                <div class="card-holder-2 do-nicescrol-2">';
                    try{
                        foreach($video->getVidos($scat['id']) as $vKey => $vid){
                           echo '<div class="card-2 do-nicescrol-2 drag-test" data-vid="'.$vid['id'].'">
                           <div class="p-1">
                               <video 
                                    id="my-video-top-'.$vid['id'].'"
                                    class="video-js"
                                    controls
                                    preload="auto"
                                    width="100%"
                                    height="128px"
                                    data-setup="{}" src="source/videos/'.$vid['link'].'" data-color="#ffffff"></video>
                           </div>
                           <span class="line-1"></span>
                           <span class="tt-3">'.$vid['name'].'</span>
                           <span class="vid-ico-bg-2 vid-right-click" data-src="source/videos/'.$vid['link'].'" data-title="'.$vid['name'].'" data-vid="'.$vid['id'].'">
                            <!--i class="fa fa-ellipsis-h text-light" aria-hidden="true"></i-->
                            <i class="fa fa-circle fa-sm text-light" style="font-size:5px;padding:0.66px;margin-top:2px" aria-hidden="true"></i>
                            <i class="fa fa-circle fa-sm text-light" style="font-size:5px;padding:0.66px;margin-top:2px" aria-hidden="true"></i>
                            <i class="fa fa-circle fa-sm text-light" style="font-size:5px;padding:0.66px;margin-top:2px" aria-hidden="true"></i>
                           </span>                               
                       </div>';
                        }
                    }catch(Exception $ex){}
        echo        '</div>
            </div>';
        
    }
    echo '<div class="card-1 pointer" data-bs-toggle="modal" data-bs-target="#exercise-type-model">
    <span class="text-cen-1">Add New & Edit</span>
</div>';
}catch(Exception $ex){
    echo '<div class="card-1 pointer" data-bs-toggle="modal" data-bs-target="#exercise-type-model">
    <span class="text-cen-1">Add New & Edit</span>
</div>';
}
    

?>