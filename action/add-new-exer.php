<?php
require_once "../inc/config.php";
require_once "../inc/FileUploader.php";
require_once "../inc/Video.php";
if(!isset($_FILES['excer-vid']) || $_FILES['excer-vid']['error'] == UPLOAD_ERR_NO_FILE) {
    echo "Please select a video file to upload.";
}else {
    if(isset($_POST['sub-cat'])){
        if(!empty($_POST['excer-name'])){
            $video = new Video($db);
            if($video->verifyName($_POST['excer-name'])){
                try{
                    $file = new FileUploader($_FILES['excer-vid'],"video");
                    $file->upload("source/videos");
                    $link = $file->getFileNameUploaded(); 
                    $dataSet = array(
                        "name" => $_POST['excer-name'],
                        "date" => date("y-m-d h:i:s"),
                        "link" => $link,
                        "sid" =>  $_POST['sub-cat']
                    );
                    try{                    
                        if($video->newVid($dataSet) == 1){
                            echo 1;
                        }else{
                            echo "Something went wrong!";
                        }
                    }catch(Exception $ex){
                        echo $ex->getMessage();
                    }        
                }catch(Exception $ex){
                    echo $ex->getMessage();
                }
            }else{
                echo "Exercise name already exists. please try another one";
            }
        }else{
            echo "Please fill out the exercise name."; 
        }
    }else{
        echo "Please select a exercise type!"; 
    }                
}
?>