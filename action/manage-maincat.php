<?php
require_once "../inc/config.php";
require_once "../inc/Video.php";
$cnew = json_decode($_POST['new']);
$cup = json_decode($_POST['update']);
$dup = json_decode($_POST['delete']);
try{
    $video = new Video($db);
    foreach($cup as $key => $val){
        try{
            $video->updateMainCat($key,$val);
        }catch(Exception $ex){
            echo $ex->getMessage();
        }
    }
}catch(Exception $ex){
    echo $ex->getMessage();
}
try{
    $video = new Video($db);
    foreach($cnew as $key => $val){
        try{
            $video->newMainCat($val);
        }catch(Exception $ex){
            echo $ex->getMessage();
        }
    }
}catch(Exception $ex){
    echo $ex->getMessage();
}
try{
    $video = new Video($db);
    foreach($dup as $key => $val){
       try{ 
            foreach($video->getSubCategories($val) as $sCatKey => $scat){
                try{               
                    foreach($video->getVidos($scat['id']) as $vKey => $vid){
                        $video->deletePlayVideo($vid['id']);
                        $video->deleteVideo($vid['id']);
                        unlink("../source/videos/".$vid['link']);
                    }
                }catch(Exception $ex){}
                $video->deleteSubCat($scat['id']);
            }            
        }catch(Exception $ex){}
        $video->deleteMainCat($val);
    }
}catch(Exception $ex){
    echo $ex->getMessage();
}
?>