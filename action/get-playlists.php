<?php
require_once "../inc/config.php";
require_once "../inc/PlayList.php";
require_once "../inc/Video.php"; 
    try{
        $playlist = new PlayList($db);
        foreach($playlist->getAll() as $plaKey => $play) { 
            $i = $play['id'];
            echo '
            <div class="col-12">
                <div class="card bg-dark-0 mb-2 playlist-card-1 ms-5">
                    <div class="card-body bg-dark-0 playlist-card-1 p-0">
                        <div class="bg-dark-0 p-3 playlist-card-1">                                    
                            <div class="d-flex flex-row plalist-expand-dec" data-my="'.$i.'" id="playcollap-'.$i.'" data-bs-toggle="collapse" href="#collapseExample-'.$i.'" role="button" aria-expanded="false" aria-controls="collapseExample">
                                <div class="playlist-ico-1">
                                    <img src="'.$play['thumbnail'].'" class=""/>
                                </div>
                                <p class="playlist-name" id="playlist-name-'.$i.'">'.$play['name'].'</p>
                                <input type="text" class="ms-5 mt-2 mb-2 my-input-1 ps-3 pe-3 d-none" value="'.$play['name'].'" id="playlist-name-edit-'.$i.'">
                                <div class="d-none animate__animated animate__faster" id="target-ex-1-'.$i.'">
                                    <div class="d-flex" id="playlits-times-'.$i.'">
                                        <p class="tt-4 ms-5 ps-4">Workout Time <span class="text-light">'.$play['w_time'].' S</span></p>
                                        <p class="tt-4 ms-4">Rest Time <span class="text-light">'.$play['r_time'].' S</span></p>
                                    </div>
                                    <div class="mt-2 d-none" id="playlits-times-edit-'.$i.'">
                                        <span class="tt-6 ms-5 ps-4">Workout Time </span><input type="number" class="my-input-2" value="'.$play['w_h'].'" id="playlist-time-edit-wh-'.$i.'" /><span class="text-light"> : </span><input type="number" class="my-input-2" value="'.$play['w_m'].'" id="playlist-time-edit-wm-'.$i.'"/><span class="text-light"> : </span><input type="number" class="my-input-2" value="'.$play['w_s'].'" id="playlist-time-edit-ws-'.$i.'"/>
                                        <span class="tt-6 ms-4 ps-4">Rest Time </span><input type="number" class="my-input-2" value="'.$play['r_h'].'" id="playlist-time-edit-rh-'.$i.'"/><span class="text-light"> : </span><input type="number" class="my-input-2" value="'.$play['r_m'].'" id="playlist-time-edit-rm-'.$i.'"/><span class="text-light"> : </span><input type="number" class="my-input-2" value="'.$play['r_s'].'" id="playlist-time-edit-rs-'.$i.'"/>
                                        
                                    </div>                                                
                                </div>
                            </div>
                            <span class="playlist-edit-ico d-none animate__animated animate__faster playlist-edit-click" data-my="'.$i.'" id="target-ex-2-'.$i.'">
                                <img src="img/pencil.png" class="pencil-ico"/>
                            </span>                                        
                        </div>
                        <div class="collapse" id="collapseExample-'.$i.'">
                            <div class="" id="playlits-content-1-'.$i.'">
                                <div class="playlist-holder-2 do-nicescrol-5">';
                                    $video2 = new Video($db);
                                    $vcount1 = 1;
                                    try{
                                        foreach($video2->playListVideo2($i) as $vKey => $vid){
                                            echo '<div class="playlist-holder-3">
                                            <div class="playlist-holder-4">
                                                <p class="tt-5 pt-2 ps-3">0'.$vcount1.'</p>                                                            
                                            </div>
                                            <div class="playlist-holder-5" style="border-radius: 5px;">
                                                <div class="card-2" data-vid="'.$vid['id'].'" style=" outline: none; 
                                                position: relative; width:100%;min-height:190px" tabindex="11">
                                                    <div class="p-1">
                                                        <video 
                                                        id="my-video-play-'.$vid['id'].'"
                                                        class="video-js"
                                                        controls
                                                        preload="auto"
                                                        width="100%"
                                                        data-setup="{}" src="'.$vid['link'].'" data-color="#ffffff"></video>
                                                        
                                                    </div>
                                                    <span class="line-1"></span>
                                                    <span class="tt-3">'.$vid['name'].'</span>
                                                    <span class="vid-ico-bg-2 vid-right-click" data-vid="'.$vid['id'].'">
                                                        <i class="fa fa-ellipsis-h text-light" aria-hidden="true"></i>
                                                    </span>                               
                                                </div>
                                            </div>
                                        </div>';
                                        $vcount1 += 1;
                                            
                                        }
                                    }catch(Exception $ex){}
                                    for ($vcount1; $vcount1 < 9 ; $vcount1++) { 
                                        echo '<div class="playlist-holder-3">
                                                <div class="playlist-holder-4">
                                                    <p class="tt-5 pt-2 ps-3">0'.$vcount1.'</p>                                                            
                                                </div>
                                                <div class="playlist-holder-5">
                                                    <span class="band-2"></span>
                                                </div>
                                            </div>';
                                    }
                                    
                                echo ' 
                                </div>
                            </div>
                            <div class="d-none" id="playlits-content-2-'.$i.'">
                                <div class="playlist-holder-2 do-nicescrol-5">';
                                    $vcount2 = 1;
                                    try{
                                        foreach($video2->playListVideo2($i) as $vKey => $vid){
                                            echo '<div class="playlist-holder-3">
                                            <div class="playlist-holder-4">
                                                <p class="tt-5 pt-2 ps-3">0'.$vcount2.'</p> 
                                                <span class="ico-remove-1" data-in="'.$i.'" data-de="'.$vcount2.'" id="vid-drop-rb-'.$vcount2.'-'.$i.'">
                                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                                </span>                                                           
                                            </div>
                                            <div class="playlist-holder-6 dropable-test" data-vid="'.$vid['id'].'" data-in="'.$i.'" data-de="'.$vcount2.'" id="vid-drop-'.$vcount2.'-'.$i.'">
                                                <div class="card-2 do-nicescrol-2" data-vid="'.$vid['id'].'" style="overflow: hidden; outline: none; 
                                                position: relative; left: 7px; top: 5px;" tabindex="11">
                                                    <div class="p-1">
                                                        <video 
                                                        id="my-video-play-edit-'.$vid['id'].'"
                                                        class="video-js"
                                                        controls
                                                        preload="auto"
                                                        width="100%"
                                                        data-setup="{}" src="'.$vid['link'].'" data-color="#ffffff"></video>
                                                    </div>
                                                    <span class="line-1"></span>
                                                    <span class="tt-3">'.$vid['name'].'</span>
                                                    <span class="vid-ico-bg-2 vid-right-click" data-vid="'.$vid['id'].'">
                                                        <i class="fa fa-ellipsis-h text-light" aria-hidden="true"></i>
                                                    </span>                               
                                                </div>
                                            </div>
                                        </div>';
                                        $vcount2 += 1;
                                            
                                        }
                                    }catch(Exception $ex){}
                                    for ($vcount2; $vcount2 < 9 ; $vcount2++) { 
                                        echo '<div class="playlist-holder-3">
                                            <div class="playlist-holder-4">
                                                <p class="tt-5 pt-2 ps-3">0'.$vcount2.'</p>  
                                                <span class="ico-remove-1 d-none" data-in="'.$i.'" data-de="'.$vcount2.'" id="vid-drop-rb-'.$vcount2.'-'.$i.'">
                                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                                </span>                                                           
                                            </div>
                                            <div class="playlist-holder-6 dropable-test" data-vid="null" data-in="'.$i.'" data-de="'.$vcount2.'" id="vid-drop-'.$vcount2.'-'.$i.'">
                                                <span class="ico-box-2"></span>
                                                <span class="ico-box-1"></span>
                                            </div>
                                        </div>';
                                    }
                                    
                                    echo' 
                                </div>
                                <div class="d-flex justify-content-between">
                                    <span class="ico-delete-1">
                                        <i class="del"></i>
                                    </span>
                                    <div class="playlist-update-btn-holder">
                                        <button class="mybtn-2 save-playlist-update" data-pid="'.$i.'">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            ';
        }
    }catch(Exception $ex){}
    ?>