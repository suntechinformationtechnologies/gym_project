<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET');
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if($_SERVER['REQUEST_METHOD'] === 'GET'){
    require_once "../../inc/config.php";
    require_once "../../inc/Video.php";
    if(isset($_GET['id'])){
        try{
            $Videos = new Video($db);
            $data = $Videos->playListVideo($_GET['id']);
        }catch(Exception $ex){
            $data = array(
                "success" => false,
                "message" => $ex->getMessage(),
                "data" => ""
            );
        }

        echo json_encode($data);
    }
    
}
?>