<?php
class Video{
    private $con;
    public function __construct($db){
        $this->con = $db;
    }
    public function playListVideo($id){
        try{
            $query = "SELECT v.*,sc.name AS sub_cat,mc.name AS m_cat,p.name AS playlist FROM video v 
            LEFT JOIN playlist_has_video phv ON phv.video_id = v.id LEFT JOIN playlist p ON 
            p.id = phv.playlist_id LEFT JOIN sub_cat sc ON sc.id = v.sub_cat_id LEFT JOIN main_cat mc 
            ON mc.id = sc.main_cat_id WHERE p.id = :id ORDER BY phv.id ASC";

            $stmt = $this->con->prepare($query);

            $id = htmlspecialchars(strip_tags($id));

            $stmt->bindParam(':id',$id);

            $stmt->execute();
            if($stmt->rowCount()>0){
                $data = array(
                    "success" => true,
                    "message" => "success",
                );
                $voct = 0;
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
                    $item = array(
                        'id' => $id,
                        'name' => $name,
                        'date' => $date,
                        'link' => BASE_URL.'videos/'.$link,
                        'playlist' => $playlist,
                        'm_cat' => $m_cat,
                        'sub_cat' => $sub_cat
                    );
                    $data2 = array(
                        $id => $item
                    );
                    array_push($data,$item);
                    $voct += 1;
                }                
                while($voct < 8){
                    $item = array(
                        'id' => 0000,
                        'name' => "Default Video",
                        'date' => "Default",
                        'link' => BASE_URL.'videos/default.mp4',
                        'playlist' => "Default",
                        'm_cat' => "Default",
                        'sub_cat' => "Default"
                    );
                    array_push($data,$item);
                    $voct += 1;
                }
                return $data;
            }else{
                //throw new Exception("Empty Result!");
                $data = array(
                    "success" => true,
                    "message" => "success",
                );
                $voct = 0;
                while($voct < 8){
                    $item = array(
                        'id' => 0000,
                        'name' => "Default Video",
                        'date' => "Default",
                        'link' => BASE_URL.'videos/default.mp4',
                        'playlist' => "Default",
                        'm_cat' => "Default",
                        'sub_cat' => "Default"
                    );
                    array_push($data,$item);
                    $voct += 1;
                }
                return $data;
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function verifyName($nam){
        try{
            $query = "SELECT * FROM `video` WHERE name = :nam";
            $stmt = $this->con->prepare($query);
            $nam = htmlspecialchars(strip_tags($nam));

            $stmt->bindParam(':nam',$nam);
            $stmt->execute();
            if($stmt->rowCount()>0){            
                return false;
            }else{
                return true;
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function newVid($data){
        try{
            $query = "INSERT `video` SET name=:name,date=:date,link=:link,sub_cat_id=:sid";

            $stmt = $this->con->prepare($query);            

            $name = htmlspecialchars(strip_tags($data['name']));
            $date = htmlspecialchars(strip_tags($data['date']));
            $link = htmlspecialchars(strip_tags($data['link']));
            $sid = htmlspecialchars(strip_tags($data['sid']));

            $stmt->bindParam(':name',$name);
            $stmt->bindParam(':date',$date);
            $stmt->bindParam(':link',$link);
            $stmt->bindParam(':sid',$sid);

            if($stmt->execute()){                
                return 1;
            }else{
                throw "Something Went wrong!";
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function playListVideo2($id){
        try{
            $query = "SELECT v.*,sc.name AS sub_cat,mc.name AS m_cat,p.name AS playlist FROM video v 
            LEFT JOIN playlist_has_video phv ON phv.video_id = v.id LEFT JOIN playlist p ON 
            p.id = phv.playlist_id LEFT JOIN sub_cat sc ON sc.id = v.sub_cat_id LEFT JOIN main_cat mc 
            ON mc.id = sc.main_cat_id WHERE p.id = :id ORDER BY phv.id ASC";

            $stmt = $this->con->prepare($query);

            $id = htmlspecialchars(strip_tags($id));

            $stmt->bindParam(':id',$id);

            $stmt->execute();
            if($stmt->rowCount()>0){
                $data = array();
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
                    $item = array(
                        'id' => $id,
                        'name' => $name,
                        'date' => $date,
                        'link' => BASE_URL.'videos/'.$link,
                        'playlist' => $playlist,
                        'm_cat' => $m_cat,
                        'sub_cat' => $sub_cat
                    );
                    $data2 = array(
                        $id => $item
                    );
                    array_push($data,$item);
                }
                return $data;
            }else{
                throw new Exception("Empty Result!");
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function getCategories(){
        try{
            $query = "SELECT * FROM `main_cat`";
            $stmt = $this->con->prepare($query);
            $stmt->execute();
            if($stmt->rowCount()>0){            
                $data = array();    
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
                    $item = array(
                        'id' => $id,
                        'name' => $name,
                    );                   
                    array_push($data,$item);
                }
                return $data;
            }else{
                throw new Exception("Empty Result!");
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function getSubCategories($id){
        try{
            $query = "SELECT * FROM `sub_cat` WHERE main_cat_id =:mid";
            $stmt = $this->con->prepare($query);
            $id = htmlspecialchars(strip_tags($id));

            $stmt->bindParam(':mid',$id);
            $stmt->execute();
            if($stmt->rowCount()>0){            
                $data = array();    
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
                    $item = array(
                        'id' => $id,
                        'name' => $name,
                    );                   
                    array_push($data,$item);
                }
                return $data;
            }else{
                throw new Exception("Empty Result!");
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function getVidos($id){
        try{
            $query = "SELECT * FROM `video` WHERE sub_cat_id =:mid";
            $stmt = $this->con->prepare($query);
            $id = htmlspecialchars(strip_tags($id));

            $stmt->bindParam(':mid',$id);
            $stmt->execute();
            if($stmt->rowCount()>0){            
                $data = array();    
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
                    $item = array(
                        'id' => $id,
                        'name' => $name,
                        'link' => $link
                    );                   
                    array_push($data,$item);
                }
                return $data;
            }else{
                throw new Exception("Empty Result!");
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function getVidosSer($id,$tag){
        try{
            $query = "SELECT * FROM `video` WHERE sub_cat_id =:mid AND name LIKE :tag";
            $stmt = $this->con->prepare($query);
            $id = htmlspecialchars(strip_tags($id));
            $tag = htmlspecialchars(strip_tags('%'.$tag.'%'));

            $stmt->bindParam(':mid',$id);
            $stmt->bindParam(':tag',$tag);
            $stmt->execute();
            if($stmt->rowCount()>0){            
                $data = array();    
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
                    $item = array(
                        'id' => $id,
                        'name' => $name,
                        'link' => $link
                    );                   
                    array_push($data,$item);
                }
                return $data;
            }else{
                throw new Exception("Empty Result!");
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function updateMainCat($id,$name){
        try{
            $query = "UPDATE `main_cat` SET name=:name WHERE id=:id";

            $stmt = $this->con->prepare($query);            

            $name = htmlspecialchars(strip_tags($name));
            $id = htmlspecialchars(strip_tags($id));

            $stmt->bindParam(':name',$name);
            $stmt->bindParam(':id',$id);

            if($stmt->execute()){                
                return true;
            }else{
                throw "Something Went wrong!";
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function newMainCat($name){
        try{
            $query = "INSERT `main_cat` SET name=:name";

            $stmt = $this->con->prepare($query);            

            $name = htmlspecialchars(strip_tags($name));

            $stmt->bindParam(':name',$name);

            if($stmt->execute()){                
                return true;
            }else{
                throw "Something Went wrong!";
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function updateSubCat($id,$name){
        try{
            $query = "UPDATE `sub_cat` SET name=:name WHERE id=:id";

            $stmt = $this->con->prepare($query);            

            $name = htmlspecialchars(strip_tags($name));
            $id = htmlspecialchars(strip_tags($id));

            $stmt->bindParam(':name',$name);
            $stmt->bindParam(':id',$id);

            if($stmt->execute()){                
                return true;
            }else{
                throw "Something Went wrong!";
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function newSubCat($name,$mid){
        try{
            $query = "INSERT INTO `sub_cat` SET name=:name,main_cat_id=:mid";

            $stmt = $this->con->prepare($query);            

            $name = htmlspecialchars(strip_tags($name));
            $mid = htmlspecialchars(strip_tags($mid));

            $stmt->bindParam(':name',$name);
            $stmt->bindParam(':mid',$mid);

            if($stmt->execute()){                
                return true;
            }else{
                throw "Something Went wrong!";
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function deletePlayVideo($id){
        try{
            $query = "DELETE FROM `playlist_has_video` WHERE video_id=:id";

            $stmt = $this->con->prepare($query);    

            $id = htmlspecialchars(strip_tags($id));

            $stmt->bindParam(':id',$id);

            if($stmt->execute()){                
                return true;
            }else{
                throw new Exception("Something went wrong!");
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function deleteVideo($id){
        try{
            $query = "DELETE FROM `video` WHERE id=:id";

            $stmt = $this->con->prepare($query);    

            $id = htmlspecialchars(strip_tags($id));

            $stmt->bindParam(':id',$id);

            if($stmt->execute()){                
                return true;
            }else{
                throw new Exception("Something went wrong!");
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function deleteSubCat($id){
        try{
            $query = "DELETE FROM `sub_cat` WHERE id=:id";

            $stmt = $this->con->prepare($query);    

            $id = htmlspecialchars(strip_tags($id));

            $stmt->bindParam(':id',$id);

            if($stmt->execute()){                
                return true;
            }else{
                throw new Exception("Something went wrong!");
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function deleteMainCat($id){
        try{
            $query = "DELETE FROM `main_cat` WHERE id=:id";

            $stmt = $this->con->prepare($query);    

            $id = htmlspecialchars(strip_tags($id));

            $stmt->bindParam(':id',$id);

            if($stmt->execute()){                
                return true;
            }else{
                throw new Exception("Something went wrong!");
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function updateVidTitle($id,$name){
        try{
            $query = "UPDATE `video` SET name=:name WHERE id=:id";

            $stmt = $this->con->prepare($query);            

            $name = htmlspecialchars(strip_tags($name));
            $id = htmlspecialchars(strip_tags($id));

            $stmt->bindParam(':name',$name);
            $stmt->bindParam(':id',$id);

            if($stmt->execute()){                
                return true;
            }else{
                throw new Exception("Something went wrong!");
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
}
?>