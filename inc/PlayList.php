<?php
class PlayList{
    private $con;
    public function __construct($db){
        $this->con = $db;
    }
    public function getAll(){
        try{
            $query = "SELECT p.*,IFNULL(p.thumbnail,'default-9.png') AS thumbnail,
            (SELECT COUNT(*) FROM playlist_has_video phv WHERE phv.playlist_id = p.id) AS vc FROM playlist p ORDER BY p.id DESC";

            $stmt = $this->con->prepare($query);

            $stmt->execute();
            if($stmt->rowCount()>0){
                $data = array();
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
                    $w_time = ($w_h * 3600) + ($w_m * 60) + ($w_s);
                    $r_time = ($r_h * 3600) + ($r_m * 60) + ($r_s);
                    $hbr = ($hb_h * 3600) + ($hb_m * 60) + ($hb_s);
                    $end_round_rest = ($eor_h * 3600) + ($eor_m * 60) + ($eor_s);
                    if($hbr > 0){
                        $hb = true;
                    }else{
                        $hb = false;
                    }
                    $item = array(
                        'id' => $id,
                        'name' => $name,
                        'date' => $date,
                        'w_time' => $w_time,
                        'r_time' => $r_time,                        
                        'thumbnail' => BASE_URL.'thumbnails/'.$thumbnail,                        
                        'vcount' => $vc,
                        'hb' => $hb,
                        'hbr' => $hbr,
                        'hbc' => $eof_slot,
                        'rounds' => $nor,
                        'end_round_rest' => $end_round_rest,
                        'w_h' => $w_h,
                        'w_m' => $w_m,
                        'w_s' => $w_s,
                        'r_h' => $r_h,
                        'r_m' => $r_m,
                        'r_s' => $r_s,
                        'hb_h' => $hb_h,
                        'hb_m' => $hb_m,
                        'hb_s' => $hb_s,
                        'eor_h' => $eor_h,
                        'eor_m' => $eor_m,
                        'eor_s' => $eor_s
                    );
                    array_push($data,$item);
                }
                 return $data;
                //throw new Exception("Fake Error!");
            }else{
                throw new Exception("Empty Result!");
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }

    public function updatePlaylist($data){
        try{
            $query = "UPDATE `playlist` SET name=:name,w_h=:w_h,w_m=:w_m,w_s=:w_s,r_h=:r_h,r_m=:r_m,r_s=:r_s,
            nor=:nor,hb_h=:hb_h,hb_m=:hb_m,hb_s=:hb_s,eor_h=:eor_h,eor_m=:eor_m,eor_s=:eor_s,eof_slot = :eof_slot, thumbnail = :imgUrl WHERE id=:id";

            $stmt = $this->con->prepare($query);    

            $id = htmlspecialchars(strip_tags($data['id']));
            $name = htmlspecialchars(strip_tags($data['name']));
            $w_h = htmlspecialchars(strip_tags($data['w_h']));
            $w_m = htmlspecialchars(strip_tags($data['w_m']));
            $w_s = htmlspecialchars(strip_tags($data['w_s']));
            $r_h = htmlspecialchars(strip_tags($data['r_h']));
            $r_m = htmlspecialchars(strip_tags($data['r_m']));
            $r_s = htmlspecialchars(strip_tags($data['r_s']));
            $nor = htmlspecialchars(strip_tags($data['nor']));
            $hb_h = htmlspecialchars(strip_tags($data['hb_h']));
            $hb_m = htmlspecialchars(strip_tags($data['hb_m']));
            $hb_s = htmlspecialchars(strip_tags($data['hb_s']));
            $eor_h = htmlspecialchars(strip_tags($data['eor_h']));
            $eor_m = htmlspecialchars(strip_tags($data['eor_m']));
            $eor_s = htmlspecialchars(strip_tags($data['eor_s']));
            $eos = htmlspecialchars(strip_tags($data['eos']));
            $imgUrl = htmlspecialchars(strip_tags($data['imgUrl']));
            

            $stmt->bindParam(':id',$id);
            $stmt->bindParam(':name',$name);
            $stmt->bindParam(':w_h',$w_h);
            $stmt->bindParam(':w_m',$w_m);
            $stmt->bindParam(':w_s',$w_s);
            $stmt->bindParam(':r_h',$r_h);
            $stmt->bindParam(':r_m',$r_m);
            $stmt->bindParam(':r_s',$r_s);
            $stmt->bindParam(':nor',$nor);
            $stmt->bindParam(':hb_h',$hb_h);
            $stmt->bindParam(':hb_m',$hb_m);
            $stmt->bindParam(':hb_s',$hb_s);
            $stmt->bindParam(':eor_h',$eor_h);
            $stmt->bindParam(':eor_m',$eor_m);
            $stmt->bindParam(':eor_s',$eor_s);
            $stmt->bindParam(':eof_slot',$eos);
            $stmt->bindParam(':imgUrl',$imgUrl);

            if($imgUrl == ""){
                $stmt->bindValue(':imgUrl', null, PDO::PARAM_STR);
            } else{
                $stmt->bindParam(':imgUrl',$imgUrl);
            }

            if($stmt->execute()){                
                return true;
            }else{
                return false;
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function deletePlayVideo($id){
        try{
            $query = "DELETE FROM `playlist_has_video` WHERE playlist_id=:id";

            $stmt = $this->con->prepare($query);    

            $id = htmlspecialchars(strip_tags($id));

            $stmt->bindParam(':id',$id);

            if($stmt->execute()){                
                return true;
            }else{
                throw false;
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function deletePlayList($id){
        try{
            $query = "DELETE FROM `playlist` WHERE id=:id";

            $stmt = $this->con->prepare($query);    

            $id = htmlspecialchars(strip_tags($id));

            $stmt->bindParam(':id',$id);

            if($stmt->execute()){                
                return true;
            }else{
                throw false;
            }
        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function addPlayVideo($pid,$vid){
        try{
            $query = "INSERT INTO `playlist_has_video` SET playlist_id=:pid,video_id=:vid";

            $stmt = $this->con->prepare($query);            

            $pid = htmlspecialchars(strip_tags($pid));
            $vid = htmlspecialchars(strip_tags($vid));

            $stmt->bindParam(':pid',$pid);
            $stmt->bindParam(':vid',$vid);

            if($stmt->execute()){                
                return true;
            }else{
                throw false;
            }

        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
    public function mewPlaylist($data){
        try{
            $query = "INSERT INTO `playlist` SET date=:date,name=:name,w_h=:w_h,w_m=:w_m,w_s=:w_s,r_h=:r_h,r_m=:r_m,r_s=:r_s,thumbnail=:thumbnail,
            nor=:nor,hb_h=:hb_h,hb_m=:hb_m,hb_s=:hb_s,eor_h=:eor_h,eor_m=:eor_m,eor_s=:eor_s,eof_slot = :eof_slot";

            $stmt = $this->con->prepare($query);            

            $date = htmlspecialchars(strip_tags($data['date']));
            $name = htmlspecialchars(strip_tags($data['name']));
            $w_h = htmlspecialchars(strip_tags($data['wh']));
            $w_m = htmlspecialchars(strip_tags($data['wm']));
            $w_s = htmlspecialchars(strip_tags($data['ws']));
            $r_h = htmlspecialchars(strip_tags($data['rh']));
            $r_m = htmlspecialchars(strip_tags($data['rm']));
            $r_s = htmlspecialchars(strip_tags($data['rs']));
            $thumbnail = htmlspecialchars(strip_tags($data['thumb']));
            $nor = htmlspecialchars(strip_tags($data['nor']));
            $hb_h = htmlspecialchars(strip_tags($data['hb_h']));
            $hb_m = htmlspecialchars(strip_tags($data['hb_m']));
            $hb_s = htmlspecialchars(strip_tags($data['hb_s']));
            $eor_h = htmlspecialchars(strip_tags($data['eor_h']));
            $eor_m = htmlspecialchars(strip_tags($data['eor_m']));
            $eor_s = htmlspecialchars(strip_tags($data['eor_s']));
            $eos = htmlspecialchars(strip_tags($data['eos']));

            $stmt->bindParam(':date',$date);
            $stmt->bindParam(':name',$name);
            $stmt->bindParam(':w_h',$w_h);
            $stmt->bindParam(':w_m',$w_m);
            $stmt->bindParam(':w_s',$w_s);
            $stmt->bindParam(':r_h',$r_h);
            $stmt->bindParam(':r_m',$r_m);
            $stmt->bindParam(':r_s',$r_s);
            $stmt->bindParam(':nor',$nor);
            $stmt->bindParam(':hb_h',$hb_h);
            $stmt->bindParam(':hb_m',$hb_m);
            $stmt->bindParam(':hb_s',$hb_s);
            $stmt->bindParam(':eor_h',$eor_h);
            $stmt->bindParam(':eor_m',$eor_m);
            $stmt->bindParam(':eor_s',$eor_s);
            $stmt->bindParam(':eof_slot',$eos);
            if($thumbnail == ""){
                $stmt->bindValue(':thumbnail', null, PDO::PARAM_STR);
            } else{
                $stmt->bindParam(':thumbnail',$thumbnail);
            }
            

            if($stmt->execute()){                
                return true;
            }else{
                throw false;
            }

        }catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
}
?>