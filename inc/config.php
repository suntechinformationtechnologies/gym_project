<?php
    date_default_timezone_set('Asia/Colombo');

    define('APP_NAME','GYM DEMO');
    define('AUTHER','Thushara Madhushan');

    define('BASE_URL','http://localhost/gym/rep/gym_project/source/');
    
    $db_user = 'root';
    $db_password = '';
    $db_name = 'gym_v2';
    $db_server = 'localhost';
    $db = new PDO('mysql:host='.$db_server.';dbname='.$db_name.';charset=utf8',$db_user,$db_password);
    //set db attributes
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES,false);
    $db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY,true);
    $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
   
?>