<?php
class FileUploader{
	//private $file;
	private $fileFormat;
	private $fileType;
	private $fileSize;
	private $targetLocation;
	private $videoFormates;
    private $imageFormates;
    //private $docFormates;
	private $fileNameOriginal;
	private $fileNameUploaded;

	private $fileTempName;
	private $fileName;
	private $dir;

    function __construct($f = array(),$tp){
        $this->fileTempName = $f['tmp_name'];
        $this->fileName = $f['name'];
        $this->fileSize = $f['size'] / 1000000;


        $this->fileFormat =  pathinfo($this->fileName, PATHINFO_EXTENSION);
		
		$this->fileNameOriginal = $this->fileName;
		$this->targetLocation = "";
		$this->imageFormates = array('jpg','jpeg','png','JPG','JPEG','PNG');
        $this->videoFormates = array("mp4","mpeg");
		$this->fileType = $this->findFileType($tp);
		$this->dir = "../";
    }

    private function findFileType($tp){
        if($tp == "image"){
            if (in_array($this->fileFormat, $this->imageFormates)) {
                return true;
            }else{
                return false;
            }
        }else{
            if (in_array($this->fileFormat, $this->videoFormates)) {
                return true;
            }else{
                return false;
            }
        }
	}    
    public function getFileNameUploaded(){
		return $this->fileNameUploaded;
	}
    
    public function upload($l){
        if($this->fileType){
            try{
                $this->targetLocation = $l;
                $location = $this->targetLocation . "/" . $this->fileNameOriginal;
                $filename = rand(10000000,99999999)."_".date("d-m-Y")."_".date("h-i-sa").".".$this->fileFormat;
                $img = $this->targetLocation."/".$filename;
                if (move_uploaded_file($this->fileTempName, $this->dir.$img)) {
                    $this->fileNameUploaded = $filename;
                    return true;
                }else{
                    throw new Exception("Something went wrong!");
                }
            }catch(Exception $ex){
                throw new Exception($ex->getMessage());
            }
        }else{
            throw new Exception("File format is not supported! ");
        }
        
	}
}
?>